import numpy as np
import utils
import dlib
import cv2


# initialize dlib's face detector (HOG-based) and then create
# the facial landmark predictor
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("./shape_predictor_68_face_landmarks.dat")

# load the input image, resize it, and convert it to grayscale
image = cv2.imread("./3.jpg")
image = utils.resize(image, width=500)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
# detect faces in the grayscale image
rects = detector(gray, 2)

face_cascade = cv2.CascadeClassifier('./haarcascades/haarcascade_frontalface_default.xml')
face_bbox = face_cascade.detectMultiScale(gray)
rect2 = utils.bbox_to_rect(face_bbox[0])

print(f'detector: {rects}')
print(f'cascada: {face_bbox}')
print('\n')

# loop over the face detections
for (i, rect) in enumerate(rects):
	# determine the facial landmarks for the face region, then
	# convert the facial landmark (x, y)-coordinates to a NumPy
	# array
	rect = rect2

	shape = predictor(gray, rect)
	shape = utils.shape_to_np(shape)
	# convert dlib's rectangle to a OpenCV-style bounding box
	# [i.e., (x, y, w, h)], then draw the face bounding box
	(i, j) = utils.FACIAL_LANDMARKS_IDXS["right_eye"]
	eye_landmarks = shape[i:j]
	eye_width = np.abs(eye_landmarks[3, 0] - eye_landmarks[0, 0])

	eye_y_center = eye_landmarks[0, 1]
	eye_bbox = (eye_y_center - (eye_width // 2),
				eye_y_center + (eye_width // 2),
				eye_landmarks[0, 0] - (eye_width // 4),
				eye_landmarks[3, 0] + (eye_width // 4))
	roi = image[eye_bbox[0]:eye_bbox[1], eye_bbox[2]:eye_bbox[3]]


	(x, y, w, h) = utils.rect_to_bbox(rect)
	cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
	# show the face number
	cv2.putText(image, "Face #{}".format(i + 1), (x - 10, y - 10),
		cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
	# loop over the (x, y)-coordinates for the facial landmarks
	# and draw them on the image
	for (x, y) in shape:
		cv2.circle(image, (x, y), 1, (0, 0, 255), -1)
# show the output image with the face detections + facial landmarks
cv2.imshow("Output", image)
cv2.imshow("roi", roi)
cv2.waitKey(0)

