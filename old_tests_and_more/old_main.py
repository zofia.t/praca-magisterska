from old_tests_and_more.EyeBasedMouseController import EyeBasedMouseController

if __name__ == '__main__':
    control = EyeBasedMouseController()

    control.calibration()

    while True:
        # If there is no point to track, then detect
        if control.num_pts < 100:
            control.detect_eye_points_to_track()
            # get new frame
            control.capture_new_frame()

        # control.track_eye()
        # TODO: get movement of the eyes

        # TODO: there the click detection should be? maybe some flag will be needed
        # if position do not change or eye disappeared:
        # control.detect_click()
        # else:
        # control.move_mouse()


