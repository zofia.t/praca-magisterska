import cv2 as cv
import numpy as np
import os

out_dir = './data/calib/test/pupil/'
os.makedirs(out_dir, exist_ok=True)

rois = sorted([out_dir + f for f in os.listdir(out_dir)
               if os.path.isfile(out_dir + f) and (f.endswith('l.png') or f.endswith('r.png'))])

for i in range(len(rois)):
    roi = cv.imread(rois[i])
    roi_gray = cv.cvtColor(roi, cv.COLOR_BGR2GRAY)

    # threshold eye image
    roi_bw = np.copy(roi_gray)
    roi_bw[roi_bw < 50] = 0  # get the darkest region and set to black
    roi_bw[roi_bw > 0] = 255  # make rest of it white
    # cv.imwrite(f'{out_dir}roi_bw_{i}.png', roi_bw)

    # opening image to fill white points inside the iris (reflections of light)
    kernel = np.ones((40, 40), np.uint8)
    opening = cv.morphologyEx(roi_bw, cv.MORPH_OPEN, kernel)
    # cv.imwrite(f'{out_dir}opening_{i}.png', opening)

    # dilatation to remove shadow of the upper eyelid
    kernel = np.ones((20, 20), np.uint8)
    dilation = cv.dilate(opening, kernel, iterations=4)
    # cv.imwrite(f'{out_dir}dilation_{i}.png', dilation)

    # expand region, to get whole iris
    kernel = np.ones((20, 20), np.uint8)
    erode = cv.erode(dilation, kernel, iterations=4)
    # cv.imwrite(f'{out_dir}erode_{i}.png', erode)

    # erode = opening.copy()

    iris_mask = np.ones_like(erode) * 255 - erode
    # cv.imwrite(f'{out_dir}iris_mask_{i}.png', iris_mask)

    # contours, _ = cv.findContours(iris_mask, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
    # for contour in contours:
    #     area = cv.contourArea(contour)
    #     rect = cv.boundingRect(contour)
    #     x, y, width, height = rect
    #     radius = 0.25 * (width + height)
    #
    #     area_condition = (50000 <= area <= 90000)
    #     symmetry_condition = (abs(1 - float(width) / float(height)) <= 0.5)
    #     fill_condition = (abs(1 - (area / (np.pi * np.power(radius, 2.0)))) <= 0.3)
    #
    #     # if area_condition and symmetry_condition and fill_condition:
    #     cv.circle(roi, (int(x + radius), int(y + radius)), int(5), (0, 180, 0), -1)
    #     cv.imwrite(f'{out_dir}circle_{i}.png', roi)
    circles = cv.HoughCircles(iris_mask, cv.HOUGH_GRADIENT, 6, 10, minRadius=100, maxRadius=160)

    if circles is not None:
        circles = np.uint16(np.around(circles))
        center_median = np.median(circles, axis=(0, 1)).astype(int)
        center_mean = np.mean(circles, axis=(0, 1)).astype(int)
        cv.circle(roi, (center_median[0], center_median[1]), 5, (0, 0, 255), -1)
        cv.circle(roi, (center_mean[0], center_mean[1]), 5, (0, 255, 255), -1)

        for idx in circles[0, :]:
            # draw the outer circle
            cv.circle(roi, (idx[0], idx[1]), idx[2], (0, 255, 0), 2)
            # draw the center of the circle
            cv.circle(roi, (idx[0], idx[1]), 5, (0, 0, 255), 3)
    cv.imwrite(f'{out_dir}circle_{i}.png', roi)
