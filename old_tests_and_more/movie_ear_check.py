from camera_eye_tracking import *
from calibration import calibration, get_dominant_eye, EAR_THRESHOLD
from time import sleep, time

if __name__ == '__main__':

    # Initialization
    # ----------------------------------------------------------------------------------------------------

    # init camera
    # camera = cv.VideoCapture(1)  # 0 back, 1 front
    # out_dir = './data/online/'
    # camera = cv.VideoCapture("./data/calib/calib_wzor_1_1080p_50cm.mp4")
    # out_dir = './data/calib/calib_wzor_1_1080p_50cm/'
    camera = cv.VideoCapture("./data/calib/piotrek_calib_1.mp4")
    # out_dir = './data/calib/piotrek_calib_1/'

    # os.makedirs(out_dir, exist_ok=True)

    # ------------------------------------------------------------------------------------------------------
    # get first two frames:
    frame = capture_frame(camera)  # capt
    # ------------------------------------------------------------------------------------------------------

    # initiate empty containers
    old_roi = np.empty((0, 0))  # for before roi region

    # bbox and rect of face:
    face_bbox = [0, 0, 0, 0]
    face_rect = utils.bbox_to_rect(face_bbox)

    # bbox of eyes and its width
    eye_bbox = [0, 0, 0, 0]
    eye_width = 0

    # counter of frames with closed eyes
    closed_eye_counter = 0

    # ------------------------------------------------------------------------------------------------------

    print("Localize face & face landmarks")
    err = 1
    while err:
        face_bbox, face_rect, err = detect_face(frame)

    _, right_eye, left_eye = detects_landmarks(frame, face_rect)

    # Left eye init:
    eye_bbox_l, _, eye_width_l = detects_roi(left_eye, frame, 0)

    # Right eye init:
    eye_bbox_r, _, eye_width_r = detects_roi(right_eye, frame, 0)

    # ------------------------------------------------------------------------------------------------------
    dominant_eye = 'right_eye'
    # ------------------------------------------------------------------------------------------------------
    name = "frame"
    cv.namedWindow(name)  # Create a named window
    cv.moveWindow(name, 0, 0)  # Move it to upper left corner


    # MAIN LOOP
    # ------------------------------------------------------------------------------------------------------
    # for i in range(0, int(camera.get(cv.CAP_PROP_FRAME_COUNT)) - 2):
    i = 0
    while camera.isOpened():
        i += 1
        start_time = time()
        # print(f"i={it}")
        all_landmarks, right_eye, left_eye = detects_landmarks(frame, face_rect)
        eye_landmark = right_eye if 'right' in dominant_eye else left_eye
        ear_ratio = ear_ratio_calc(eye_landmark)

        roi = utils.select_bbox_region_and_reshape(frame, eye_bbox_r)
        cv.imshow('roi', roi)

        mask = create_iris_mask(roi)

        # cv.imshow('mask', mask)

        percent_of_black = np.sum(mask > 0) / (roi.shape[0] * roi.shape[1])
        # average_sum = np.sum(roi) / (roi.shape[0] * roi.shape[1])

        # face rectangle:
        x, y, w, h = face_bbox
        cv.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)

        # face landmark
        for (xl, yl) in all_landmarks:
            cv.circle(frame, (xl, yl), 5, (0, 0, 255), -1)

        x, y = 10, 800
        cv.putText(frame, f"{i}; EAR ratio: {np.round(ear_ratio, 4)}", (x, y - 100),
                   cv.FONT_HERSHEY_SIMPLEX, 1, (209, 80, 0, 255), 1)
        cv.putText(frame, f"{i}; percent of black pixels: {np.round(percent_of_black, 4)}", (x, y - 50),
                   cv.FONT_HERSHEY_SIMPLEX, 1, (209, 80, 0, 255), 1)
        # cv.putText(frame, f"{i}; min pixel value: {np.min(roi)}", (x, y - 60),
        #            cv.FONT_HERSHEY_SIMPLEX, 1, (209, 80, 0, 255), 1)
        # if check_if_eye_closed(eye_bbox_l, eye_bbox_r, frame, left_eye, right_eye):
        if ear_ratio < 0.25:
            closed_eye_counter += 1
            cv.putText(frame, f"EYE CLOSED!  counter: {closed_eye_counter}", (x, y ),
                       cv.FONT_HERSHEY_SIMPLEX, 4, (209, 80, 0, 255), 2)
        else:
            closed_eye_counter = 0

        cv.imshow(name, frame)

        # get new frame
        old_frame, frame = capture_new_frame(frame, camera)
        sleep(0.06)
        if cv.waitKey(1) == ord('q'):
            break

    # When everything done, release the capture
    camera.release()

    cv.destroyAllWindows()
