import numpy as np
import pynput
import cv2 as cv
import dlib
import utils
# import pyoptflow

# TODO: choose right start position == center of the screen
START_MOUSE_X_POSITION = 100
START_MOUSE_Y_POSITION = 100


class EyeBasedMouseController:
    def __init__(self, cam_id=1, optical_flow_method=cv.calcOpticalFlowPyrLK):
        # init mouse controller and select start point
        self.mouse = pynput.mouse.Controller()
        self.mouse.position = (START_MOUSE_X_POSITION, START_MOUSE_Y_POSITION)

        # init camera
        self.camera = cv.VideoCapture(cam_id)  # 0 back, 1 front

        # init detectors
        # TODO: decide which to use (which is the best)
        self.face_cascade = cv.CascadeClassifier('./haarcascades/haarcascade_frontalface_default.xml')
        # self.eye_cascade = cv.CascadeClassifier('./haarcascades/haarcascade_eye.xml')
        # self.right_eye_cascade = cv.CascadeClassifier('./haarcascades/haarcascade_righteye_2splits.xml')
        # self.left_eye_cascade = cv.CascadeClassifier('./haarcascades/haarcascade_lefteye_2splits.xml')
        self.face_detector = dlib.get_frontal_face_detector()

        self.landmark_predictor = dlib.shape_predictor("./shape_predictor_68_face_landmarks.dat")

        # set parameters for ShiTomasi corner detection
        # TODO: parameters from inż project - check them
        self.feature_params = dict(maxCorners=5000,
                                   qualityLevel=0.1,
                                   minDistance=2,
                                   blockSize=30)

        # init opticalFlow method or parameters
        # TODO: define parameters to different methods
        # Parameters for Lucas Kanade optical flow
        self.lk_params = dict(winSize=(20, 20),  # window size
                              maxLevel=2,  # piramida level (?)
                              criteria=(cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 10, 0.03))  # criteria of stop

        # save method name as parameter
        self.optical_flow_fun = optical_flow_method

        # others
        self.old_frame = self.capture_frame()  # capture first one frame
        self.frame = self.capture_frame()  # capture current frame
        self.roi = []
        self.bbox_points = np.empty((0, 2), int)
        self.num_pts = 0
        self.old_points = np.empty((0, 2))

        self.face_bbox = np.zeros(4, int)
        self.face_rect = utils.bbox_to_rect(self.face_bbox)
        self.all_landmarks = np.zeros((68, 2), int)
        self.eye_landmarks = np.zeros((6, 2), int)
        self.eye_bbox = np.zeros(4, int)
        self.eye_width = 0
        self.dominant_eye = "right_eye"

    def calibration(self):
        # TODO: calibrate:
        #  for example rich each corner of the monitor by eyes and check the distances (x,y positions?)
        #  and calculate the coefficient (in order to mouse max/min position)
        # TODO: check if there is no dominant eye
        pass

    def capture_frame(self):
        # Check if camera is opened
        assert self.camera.isOpened(), "Camera is closed!"

        # Capture one frame
        ret, frame = self.camera.read()
        assert ret, "Can't receive frame"
        # return in gray scale
        return cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

    def capture_new_frame(self):
        self.old_frame = np.copy(self.frame)
        self.frame = self.capture_frame()

    def get_frame(self):
        return self.frame

    def detect_face(self):
        # detect face by Viola-Jones cascade
        face_bboxs = self.face_cascade.detectMultiScale(self.frame)
        if not (len(face_bboxs) == 0):
            self.face_bbox = face_bboxs[0]
            self.face_rect = utils.bbox_to_rect(self.face_bbox)
            return 0
        else:
            return 1

        # detect face by HOG-based detector
        # face_rects = self.face_detector(self.frame, 1)
        # if not (len(face_rects) == 0):
            # self.face_rect = face_rects[0]
            # self.face_bbox = utils.rect_to_bbox(self.face_rect)
            # return 0
        # else:
        #     return 1

    def detects_eyes(self):
        self.all_landmarks = self.landmark_predictor(self.frame, self.face_rect)
        self.all_landmarks = utils.shape_to_np(self.all_landmarks)
        (i, j) = utils.FACIAL_LANDMARKS_IDXS[self.dominant_eye]
        self.eye_landmarks = self.all_landmarks[i:j]

    def detects_roi(self):
        self.eye_width, _ = np.max(self.eye_landmarks, axis=0) - np.min(self.eye_landmarks, axis=0)
        eye_center_x, eye_center_y = np.mean(self.eye_landmarks, axis=0, dtype=int)

        self.eye_bbox = [eye_center_x - ((self.eye_width + self.eye_width // 10) // 2),
                         eye_center_y - (self.eye_width // 3),
                         self.eye_width + self.eye_width // 10,
                         (self.eye_width // 3) * 2]

        self.roi = self.frame[self.eye_bbox[0]:self.eye_bbox[0] + self.eye_bbox[2],
                              self.eye_bbox[1]:self.eye_bbox[1] + self.eye_bbox[3]]

    def visualize(self):
        img = np.copy(self.frame)

        # face rectangle:
        x, y, w, h = self.face_bbox
        cv.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)

        # face landmark
        for (x, y) in self.all_landmarks:
            cv.circle(img, (x, y), 1, (0, 0, 255), -1)

        # eye rectangle:
        ex, ey, ew, eh = self.eye_bbox
        cv.rectangle(img, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)

        cv.imshow('frame', img)
        # cv.imwrite('./image.png', img)

        # # points to track:
        # for n in range(points.shape[0]):
        #     px, py = points[n][0]
        #     cv.circle(face_area, (int(px + x), int(py + y)), 2, (100, 25, 200), 1)

    def detect_eye_points_to_track(self, vizualization=1):
        err = self.detect_face()
        if not err:
            self.detects_eyes()
            self.detects_roi()

            # # TODO: isint it good idea to save save points where is eye iris? the darkest points in eye
            # points = cv.goodFeaturesToTrack(self.roi, mask=None, **self.feature_params)
            #
            # x, y, w, h = self.eye_bbox
            # # TODO: check correctness and add coments
            # if points.shape[0] > 0:
            #     for n in range(points.shape[0]):
            #         px, py = points[n][0]
            #         points[n][0] = np.array([px + x, py + y])

            # self.num_pts = points.shape[0]
            # self.old_points = points.copy()
            # self.bbox_points = np.array([[x, y], [x + w, y], [x + w, y + h], [x, y + h]])

            # test visualization:
            if vizualization:
                self.visualize()

    def track_eye(self):
        # TODO: check and comments (cod from inż)
        points, st, _ = self.optical_flow_fun(self.old_frame, self.frame, self.old_points, None, **self.lk_params)

        visible_points = points[st == 1].copy()
        visible_points = np.reshape(visible_points, (1, -1, 2))

        old_inliers = self.old_points[st == 1].copy()
        old_inliers = np.reshape(old_inliers, (1, -1, 2))
        num_pts = visible_points.shape[1]

        if num_pts >= 10:
            transformation = cv.estimateRigidTransform(src=old_inliers,
                                                        dst=visible_points,
                                                        fullAffine=False)

            if transformation is not None:
                translate_vector = transformation[:, 2].reshape((2, 1))
                rotation_matrix = transformation[:, :2]
                self.bbox_points = rotation_matrix.dot(self.bbox_points.T + translate_vector)
                self.bbox_points = self.bbox_points.T
            visible_points = np.reshape(visible_points, (-1, 1, 2))
            self.old_points = visible_points.copy()  # reshape to has the same shape as before

    def move_mouse(self):
        # TODO: where are define dx and dy?
        # self.mouse.move(dx, dy)
        pass

    def detect_click(self):
        # TODO: recognise blinking or winking in order to manage click
        # if the eye is closed then new point would be not find, even eye can be not recognised
        # there should be threshold to measure the time and decide if it was click or just blink
        # self.mouse.click()
        pass
