from camera_eye_tracking import detects_landmarks, detect_face, track_eye_movement
import cv2 as cv
import numpy as np

out_dir = './data/calib_v2/test_5/'
frame = cv.imread(f'{out_dir}frame.png')
frame = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

roi = cv.imread(f'{out_dir}right_eye.png')
# roi = cv.imread(f'{out_dir}left_eye.png')
roi = cv.cvtColor(roi, cv.COLOR_BGR2GRAY)

_, face_rect, _ = detect_face(frame)
_, right_eye, left_eye = detects_landmarks(frame, face_rect)

eye_width_r, eye_height_r = np.max(right_eye, axis=0) - np.min(right_eye, axis=0)
eye_width_l, eye_height_l = np.max(left_eye, axis=0) - np.min(left_eye, axis=0)

eye_center_x_r, eye_center_y_r = np.mean(right_eye, axis=0, dtype=int)
eye_center_x_l, eye_center_y_l = np.mean(left_eye, axis=0, dtype=int)

eye_bbox_r = [eye_center_x_r - ((eye_width_r + eye_width_r // 2) // 2),  # x position is more then 1/2 od width from center
            eye_center_y_r - ((eye_width_r + eye_width_r // 5) // 2),  # y position is calculated no matter of eye status
            eye_width_r + eye_width_r // 2,
            eye_width_r + eye_width_r // 5]

roi_r = frame[eye_bbox_r[1]:eye_bbox_r[1] + eye_bbox_r[3],  # y:y+h
      eye_bbox_r[0]:eye_bbox_r[0] + eye_bbox_r[2]]  # x:x+w

eye_bbox_l = [eye_center_x_l - ((eye_width_l + eye_width_l // 2) // 2),  # x position is more then 1/2 od width from center
            eye_center_y_l - ((eye_width_l + eye_width_l // 5) // 2),  # y position is calculated no matter of eye status
            eye_width_l + eye_width_l // 2,
            eye_width_l + eye_width_l // 5]

roi_l = frame[eye_bbox_l[1]:eye_bbox_l[1] + eye_bbox_l[3],  # y:y+h
      eye_bbox_l[0]:eye_bbox_l[0] + eye_bbox_l[2]]  # x:x+w

width = 640
scale_r = width / float(roi_r.shape[0])
scale_l = width / float(roi_l.shape[0])

eye_width_r = int(eye_width_r * scale_r)
eye_height_r = int(eye_height_r * scale_r)

eye_width_l = int(eye_width_l * scale_l)
eye_height_l = int(eye_height_l * scale_l)

# set parameters of the function which determines corners
feature_params = dict(maxCorners=1000,
                      qualityLevel=0.005,
                      minDistance=1,
                      blockSize=5,
                      useHarrisDetector=False,
                      k=0.04)

# mask = create_iris_mask(roi, save=1,  out_dir=out_dir)

mask = np.ones((roi.shape[0], roi.shape[1]), dtype=np.uint8) * 0
start_x = mask.shape[1] // 2 - eye_width_r // 2
start_y = mask.shape[0] // 2 - eye_height_r // 2
end_x = mask.shape[1] // 2 + eye_width_r // 2
end_y = mask.shape[0] // 2 + eye_height_r // 2

mask = cv.rectangle(mask, (start_x, start_y), (end_x, end_y), (255, 255, 255), -1)

cv.imwrite(f'{out_dir}roi_mask.png', cv.add(roi, mask))

# look for features around the iris
points = cv.goodFeaturesToTrack(roi, mask=mask, **feature_params)
save_roi = np.copy(roi)
save_roi = cv.cvtColor(save_roi, cv.COLOR_GRAY2BGR)

for x, y in points[:, 0, :]:
    save_roi = cv.circle(save_roi, (x, y), 5, (100, 25, 200), -1)

cv.imwrite(f'{out_dir}points.png', save_roi)
