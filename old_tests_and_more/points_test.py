import numpy as np
import matplotlib.pyplot as plt

plt.figure()
plt.plot(eye_position_r[:, 0], eye_position_r[:, 1], 'ro')
plt.plot(eye_position_l[:, 0], eye_position_l[:, 1], 'bo')

for i in range(eye_position_l.shape[0]):
    print(f'i: {i}\neye_left: {eye_position_l[i]}\neye_right: {eye_position_r[i]}\n')
    plt.annotate(f'{i}', (eye_position_l[i, 0], eye_position_l[i, 1]))
    plt.annotate(f'{i}', (eye_position_r[i, 0], eye_position_r[i, 1]))

plt.grid(True)
plt.show()
