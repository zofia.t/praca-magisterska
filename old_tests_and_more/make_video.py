import numpy as np
from camera_eye_tracking import capture_frame
import cv2 as cv
cap = cv.VideoCapture(1)
# Define the codec and create VideoWriter object
fourcc = cv.VideoWriter_fourcc(*'XVID')
out = cv.VideoWriter('output.avi', fourcc, 20.0, (640,  480))
while cap.isOpened():
    frame = capture_frame(cap)
    frame = cv.cvtColor(frame, cv.COLOR_GRAY2BGR)
    out.write(frame)
    cv.imshow('frame', frame)
    if cv.waitKey(1) == ord('q'):
        break
# Release everything if job is finished
cap.release()
out.release()
