from camera_eye_tracking import capture_frame, detects_landmarks, detect_face
from camera_eye_tracking import detects_roi, create_iris_mask, ear_ratio_calc
from controll_cursor import CENTER_X_POSITION, CENTER_Y_POSITION, SCREEN_WIDTH, SCREEN_HEIGHT
import utils
import cv2 as cv
import numpy as np
from time import sleep
import pickle
from sklearn.linear_model import LinearRegression

EAR_THRESHOLD = 0.25
RADIUS = 20
CALIBRATION_POINTS = np.array([[0 + (2 * RADIUS), 0 + (2 * RADIUS)],
                               [CENTER_X_POSITION, CENTER_Y_POSITION],
                               [SCREEN_WIDTH - (2 * RADIUS), 0 + (2 * RADIUS)],
                               [SCREEN_WIDTH - (2 * RADIUS), SCREEN_HEIGHT - (2 * RADIUS)],
                               [CENTER_X_POSITION, CENTER_Y_POSITION],
                               [0 + (2 * RADIUS), SCREEN_HEIGHT - (2 * RADIUS)],
                               [CENTER_X_POSITION, CENTER_Y_POSITION]])

# TODO: more points and maybe random generated?
# TODO: maybe number of points should be designated by score of models? and min and max of points should be done
# TODO: maybe points should be not in the corners but like win menu - with some space from edge of the screen


def calibration(camera, frame, face_rect, out_movie,
                number_of_points=CALIBRATION_POINTS.shape[0], out_dir='./data/calib/test/'):
    global EAR_THRESHOLD
    # INIT
    # ---------------------------------------------------------------------
    _, right_eye, left_eye = detects_landmarks(frame, face_rect)

    # EAR ratio for each and both eyes
    # ear_ratio = get_mean_ear_ratio_for_both_eyes(left_eye, right_eye)

    # Left eye init:
    eye_bbox_l, _, _ = detects_roi(left_eye, frame, 0)
    eye_position_l = np.zeros((number_of_points, 2))

    # Right eye init:
    eye_bbox_r, _, _ = detects_roi(right_eye, frame, 0)
    eye_position_r = np.zeros((number_of_points, 2))

    closed = True
    closed_eye_counter = 0

    # Create a named window
    calib_win_name = "calibration"
    cv.namedWindow(calib_win_name, cv.WINDOW_NORMAL)
    # Set full screen
    cv.setWindowProperty(calib_win_name, cv.WND_PROP_FULLSCREEN, cv.WINDOW_FULLSCREEN)
    # blank white image in size of screen
    show_point = np.ones((SCREEN_HEIGHT, SCREEN_WIDTH, 3), np.uint8) * 255
    cv.imshow(calib_win_name, show_point)
    for i in range(3):  # for 0.1s white screen
        cv.imshow(calib_win_name, show_point)
        frame = capture_frame(camera)
        save_frame(frame, out_movie)
        if cv.waitKey(1) == ord('q'):
            break
    # sleep(1)
    # ---------------------------------------------------------------------
    # Calculate EAR threshold
    EAR_THRESHOLD = calculate_ear_threshold(camera, face_rect, frame, show_point, out_movie, out_dir)
    frame = capture_frame(camera)
    save_frame(frame, out_movie)
    # ---------------------------------------------------------------------

    # LOOP over calibration points
    for n in range(number_of_points):
        # select point on screen to be reach be eye
        point_to_reach = CALIBRATION_POINTS[n]
        # visualize point
        visualize_calibration_point(point_to_reach, np.copy(show_point), calib_win_name)
        if cv.waitKey(1) == ord('q'):
            break
        print(f'point to reach: {point_to_reach}')
        reach_point = 0

        # Wait until open eyes
        while closed:
            # print('waiting to open eyes')
            # get the eyes landmarks
            _, right_eye, left_eye = detects_landmarks(frame, face_rect)
            closed = check_if_eye_closed(left_eye, right_eye)

            # get new frame
            frame = capture_frame(camera)
            save_frame(frame, out_movie)

        # set geometric center of the eyes to not positives - to catch errors
        geometric_center_l = np.ones((30, 2)) * -1
        geometric_center_r = np.ones((30, 2)) * -1

        eye_frames_l = np.zeros((640, int(eye_bbox_l[2] * (640 / float(eye_bbox_l[3]))), 30))
        eye_frames_r = np.zeros((640, int(eye_bbox_r[2] * (640 / float(eye_bbox_r[3]))), 30))

        # Loop to reach point - eyes closed with enough time
        while (not reach_point) or (not closed_eye_counter):
            closed = check_if_eye_closed(left_eye, right_eye)

            if not closed:
                # If eyes are open:
                closed_eye_counter = 0

                # get left eye center and region
                geom_center_l_buff, eye_roi_frame_l = get_geometric_center_of_eye(eye_bbox_l, frame, out_dir)
                # get right eye center and region
                geom_center_r_buff, eye_roi_frame_r = get_geometric_center_of_eye(eye_bbox_r, frame, out_dir)

                # if not calculated do not save
                if np.isfinite(geom_center_l_buff).all():
                    geometric_center_l[:-1] = geometric_center_l[1:]
                    geometric_center_l[-1] = geom_center_l_buff
                    eye_frames_l[:, :, :-1] = eye_frames_l[:, :, 1:]
                    eye_frames_l[:, :, -1] = eye_roi_frame_l

                if np.isfinite(geom_center_r_buff).all():
                    geometric_center_r[:-1] = geometric_center_r[1:]
                    geometric_center_r[-1] = geom_center_r_buff
                    eye_frames_r[:, :, :-1] = eye_frames_r[:, :, 1:]
                    eye_frames_r[:, :, -1] = eye_roi_frame_r
            else:
                # If eye are closed
                closed_eye_counter += 1

                if (closed_eye_counter > 3) and (closed_eye_counter < 13):  # 3-13 frames (0.1-0.4 s) - natural blinking
                    pass  # nothing happening
                elif (closed_eye_counter > 16) and (closed_eye_counter < 33):  # 16-33 frames (0.5-1 s) - unnatural blinking
                    reach_point = 1
                    print('point reached')
                elif (closed_eye_counter > 33) and (closed_eye_counter < 100):  # 33-100 frames (1-3 s) long blinking
                    pass  # nothing happening
                else:   # really long blinking - something is wrong
                    pass

            # get the eyes landmarks
            _, right_eye, left_eye = detects_landmarks(frame, face_rect)
            # calculate EAR ratios
            ear_ratio = get_mean_ear_ratio_for_both_eyes(left_eye, right_eye)

            # get new frame
            frame = capture_frame(camera)
            save_frame(frame, out_movie)

        # save position of both eyes
        for idx in range(20):
            if eye_frames_l[:, :, idx].any():
                cv.imwrite(f'{out_dir}{n}/{idx}_l.png', eye_frames_l[:, :, idx])
            if eye_frames_r[:, :, idx].any():
                cv.imwrite(f'{out_dir}{n}/{idx}_r.png', eye_frames_r[:, :, idx])

        eye_position_l[n] = np.mean(geometric_center_l[np.all(geometric_center_l > -1, axis=1)][:20], axis=0)
        eye_position_r[n] = np.mean(geometric_center_r[np.all(geometric_center_r > -1, axis=1)][:20], axis=0)
        print(f'center left: {eye_position_l[n]}\ncenter right: {eye_position_r[n]}')

    # close calibration points
    cv.destroyWindow(calib_win_name)

    np.save(f'{out_dir}eye_position_l.npy', eye_position_l)
    np.save(f'{out_dir}eye_position_r.npy', eye_position_r)
    np.save(f'{out_dir}calibration_points.npy', CALIBRATION_POINTS)

    # prepare models in order to distances between points
    models = prepare_models(eye_position_l, eye_position_r, number_of_points)

    # save_models
    for model in models:
        file_name = out_dir + model['eye']
        pickle.dump(model['model'], open(file_name + '_model.pkl', 'wb'))  # model to pickle
        np.save(f'{file_name}_x.npy', model['x'])  # input of model
        np.save(f'{file_name}_y.npy', model['y'])  # output of model

    # in order to score of the models return the best one and the dominant eye
    # dominant_eye, calibration_model = get_dominant_eye(models)
    return get_dominant_eye(models)  # dominant_eye, calibration_model


def save_frame(frame, out_movie, save=True):
    if save:
        frame = cv.cvtColor(frame, cv.COLOR_GRAY2BGR)
        frame = cv.flip(frame, 1)
        out_movie.write(frame)
    return


def calculate_ear_threshold(camera, face_rect, frame, show_point, out_movie, out_dir='./data/calib/test/',
                            ear_calibration_time=60):  # 60 frames = 2 s
    # First get the ear for open eyes
    ear_open, frame = get_ear_th_for_open_or_closed_eyes(camera, ear_calibration_time, face_rect, frame,
                                                         out_movie, show_point, gesture='open')

    ear_closed, frame = get_ear_th_for_open_or_closed_eyes(camera, ear_calibration_time, face_rect, frame,
                                                           out_movie, show_point, gesture='closed')

    ear_th = ear_closed + (ear_open - ear_closed) / 6
    print(f'EAR threshold {ear_th}')
    np.save(f'{out_dir}ear_th.npy', np.array([ear_open, ear_closed, ear_th]))
    return ear_th


def get_ear_th_for_open_or_closed_eyes(camera, ear_calibration_time, face_rect, frame, out_movie, show_point, gesture):
    if gesture == 'open':
        command = "Keep yours eyes wide open for 2s"
        out_ear_function = np.max
    elif gesture == 'closed':
        command = "Keep yours eyes closed for 2s"
        out_ear_function = np.min
    else:  # error
        return -1, frame

    ear_buffer = np.zeros(ear_calibration_time)
    for i in range(ear_calibration_time):  # for 2s period
        visualize_calibration_command(np.copy(show_point), command)
        # cv.imwrite(f'{out_dir}{gesture}.png', frame)

        # get the eyes landmarks
        _, right_eye, left_eye = detects_landmarks(frame, face_rect)
        ear_buffer[i] = get_mean_ear_ratio_for_both_eyes(left_eye, right_eye)
        # get new frame
        frame = capture_frame(camera)
        save_frame(frame, out_movie)

        if cv.waitKey(1) == ord('q'):
            break

    # cv.imwrite(f'{out_dir}last_{gesture}.png', frame)

    ear_th = out_ear_function(ear_buffer)  # get EAR
    print(f'EAR {gesture} eyes: {ear_th}')
    for i in range(3):  # for 0.1s black screen
        cv.imshow("calibration", np.copy(show_point) * 0)
        frame = capture_frame(camera)
        save_frame(frame, out_movie)

        if cv.waitKey(1) == ord('q'):
            break
    return ear_th, frame


def check_if_eye_closed(left_eye, right_eye):
    # calculate EAR ratio
    ear_ratio = get_mean_ear_ratio_for_both_eyes(left_eye, right_eye)

    return ear_ratio < EAR_THRESHOLD


def calculate_percent_of_black(eye_bbox, frame):
    roi = utils.select_bbox_region_and_reshape(frame, eye_bbox)

    mask = create_iris_mask(roi)
    return np.sum(mask > 0) / (roi.shape[0] * roi.shape[1])


def visualize_calibration_point(point_to_reach, show_point, name="calibration"):
    # Add red point to screen
    show_point = cv.circle(show_point, (point_to_reach[0], point_to_reach[1]), RADIUS, (0, 0, 255), -1)
    cv.imshow(name, show_point)


def visualize_calibration_command(show_point, command, name="calibration", font=cv.FONT_HERSHEY_SIMPLEX):
    # get boundary of this text
    command_size = cv.getTextSize(command, font, 2, 2)[0]

    # get coords based on boundary
    command_x = (show_point.shape[1] - command_size[0]) // 2
    command_y = (show_point.shape[0] + command_size[1]) // 2

    # Show command on center of screen
    cv.putText(show_point, f"{command}", (command_x, command_y), font, 2, (0, 0, 0), 2)
    cv.imshow(name, show_point)


def get_mean_ear_ratio_for_both_eyes(left_eye, right_eye):
    # get EAR ratio for each eye
    ear_ratio_l = ear_ratio_calc(left_eye)
    ear_ratio_r = ear_ratio_calc(right_eye)
    # calculated mean EAR
    # ear_ratio = np.mean([ear_ratio_l, ear_ratio_r])
    # return ear_ratio
    return np.max([ear_ratio_l, ear_ratio_r])


def get_geometric_center_of_eye(eye_bbox, frame, out_dir):

    roi = utils.select_bbox_region_and_reshape(frame, eye_bbox)

    # get mask of iris using basic morphology functions
    iris_mask = create_iris_mask(roi, save=1, out_dir=out_dir)

    # iris_x, iris_y = np.where(iris_mask)  # get coordinates where is iris
    #
    # if iris_x.shape[0]:  # if any
    #     iris_min_x = np.min(iris_x)
    #     iris_max_x = np.max(iris_x)
    #     iris_min_y = np.min(iris_y)
    #     iris_max_y = np.max(iris_y)
    #     # calculate geometric center of the iris position
    #     geometric_center = [iris_min_x + (iris_max_x - iris_min_x), iris_min_y + (iris_max_y - iris_min_y)]
    # else:
    #     # not found (too much dilatation ect)
    #     geometric_center = [np.inf, np.inf]
    # return geometric_center, roi

    circles = cv.HoughCircles(iris_mask, cv.HOUGH_GRADIENT, 6, 10, minRadius=100, maxRadius=160)

    if circles is not None:
        circles = np.uint16(np.around(circles))
        center = np.median(circles, axis=(0, 1)).astype(int)
        # cv.circle(roi, (center[0], center[1]), 5, (0, 0, 255), -1)
        return center[:2], roi
    else:
        return [np.inf, np.inf], roi


def prepare_models(eye_position_l, eye_position_r, number_of_points):
    # get the distances between points on screen and reach by each eye
    dist_1d_screen = get_distances_of_points(CALIBRATION_POINTS[:number_of_points])
    dist_1d_l = get_distances_of_points(eye_position_l)
    dist_1d_r = get_distances_of_points(eye_position_r)

    # generate linear regression model for each eye
    models_1d_l = get_calibration_model(dist_1d_screen, dist_1d_l)
    models_1d_r = get_calibration_model(dist_1d_screen, dist_1d_r)

    # pack them into list
    models = [{'model': models_1d_l, 'x': dist_1d_l, 'y': dist_1d_screen, 'eye': 'left_eye'},
              {'model': models_1d_r, 'x': dist_1d_r, 'y': dist_1d_screen, 'eye': 'right_eye'}]

    return models


def get_distances_of_points(points):
    # separate x and y dimension
    width = points[:, 0].reshape(-1, 1)
    height = points[:, 1].reshape(-1, 1)

    # get all distances in one dimension (width or height)
    width_dist = (width.T - width).reshape(-1)
    height_dist = (height.T - height).reshape(-1)

    return width_dist, height_dist


def get_calibration_model(distances_on_screen, distances):
    # get x, y points for width
    width_x = distances[0].reshape(-1, 1)
    width_y = distances_on_screen[0]

    # get x, y points for height
    height_x = distances[1].reshape(-1, 1)
    height_y = distances_on_screen[1]

    # calculate regression for each (width and height) dimension
    model_width = LinearRegression().fit(width_x, width_y)
    model_height = LinearRegression().fit(height_x, height_y)
    return model_width, model_height


def get_dominant_eye(models):
    # prepare array to contain the models scores
    scores = np.zeros(len(models))

    # get score for each model:
    for i in range(len(models)):
        scores[i] = get_score_of_model(models[i]['x'], models[i]['y'], models[i]['model'])

    # the biggest score is the best model and the dominant eye is the corresponding one
    return models[np.argmax(scores)]['eye'], models[np.argmax(scores)]['model']


def get_score_of_model(x, y, model, width=0, height=1):
    score_width = model[width].score(x[width].reshape(-1, 1), y[width])
    score_height = model[height].score(x[height].reshape(-1, 1), y[height])
    # return the mean score for each direction
    return np.mean([score_width, score_height])



if __name__ == '__main__':
    # INITIALIZATION
    # ----------------------------------------------------------------------------------------------------
    # init camera
    # camera = cv.VideoCapture("./data/calib/calib_2.mp4")
    camera = cv.VideoCapture(1)
    # camera = cv.VideoCapture("./data/calib/piotrek_calib_1.mp4")
    # camera = cv.VideoCapture("./data/calib/calib_with_ear_th.mp4")
    # camera = cv.VideoCapture("./data/calib/test/output_7.avi")

    # Save out video
    fourcc = cv.VideoWriter_fourcc(*'DIVX')
    w = int(camera.get(cv.CAP_PROP_FRAME_WIDTH))
    h = int(camera.get(cv.CAP_PROP_FRAME_HEIGHT))
    fps = camera.get(cv.CAP_PROP_FPS)
    out = cv.VideoWriter('./data/calib/test/output_8.avi', fourcc, 30.0, (w, h))

    # get first frame:
    frame = capture_frame(camera)  # capt
    save_frame(frame, out)

    # bbox and rect of face:
    face_bbox = [0, 0, 0, 0]
    face_rect = utils.bbox_to_rect(face_bbox)

    # FIND FACE IN A FIRST FRAME
    # ----------------------------------------------------------------------------------------------------
    err = 1
    while err:
        face_bbox, face_rect, err = detect_face(frame)

    # CALIBRATION
    # ----------------------------------------------------------------------------------------------------
    dominant_eye, calibration_model = calibration(camera, frame, face_rect, out)
    # ----------------------------------------------------------------------------------------------------

    while camera.isOpened():
        frame = capture_frame(camera)  # capt
        save_frame(frame, out)

        if cv.waitKey(1) == ord('q'):
            break

    camera.release()
    cv.destroyAllWindows()
