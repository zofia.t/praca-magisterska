import numpy as np
import cv2 as cv
import dlib
import utils

if __name__ == '__main__':

    cap = cv.VideoCapture(1)

    face_cascade = cv.CascadeClassifier('./haarcascades/haarcascade_frontalface_default.xml')
    eye_cascade = cv.CascadeClassifier('./haarcascades/haarcascade_eye.xml')

    face_detector = dlib.get_frontal_face_detector()
    landmark_predictor = dlib.shape_predictor("./shape_predictor_68_face_landmarks.dat")

    if not cap.isOpened():
        print("Cannot open camera")
        exit()
    while True:
        # Capture frame-by-frame
        ret, frame = cap.read()
        # if frame is read correctly ret is True
        if not ret:
            print("Can't receive frame (stream end?). Exiting ...")
            break
        # Our operations on the frame come here
        gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        # Display the resulting frame
        cv.imshow('frame', gray)

        img = np.copy(frame)
        faces = face_cascade.detectMultiScale(gray, 1.3, 5)
        for (x, y, w, h) in faces:
            cv.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
            roi_gray = gray[y:y + h, x:x + w]
            roi_color = img[y:y + h, x:x + w]

            face_rect = utils.bbox_to_rect([x, y, w, h])

            all_landmarks = landmark_predictor(gray, face_rect)
            all_landmarks = utils.shape_to_np(all_landmarks)

            for (x, y) in all_landmarks:
                cv.circle(img, (x, y), 1, (0, 0, 255), -1)

            (i, j) = utils.FACIAL_LANDMARKS_IDXS["left_eye"]
            eye_landmarks = all_landmarks[i:j]

            eye_width, eye_high = np.max(eye_landmarks, axis=0) - np.min(eye_landmarks, axis=0)
            eye_center_x, eye_center_y = np.mean(eye_landmarks, axis=0, dtype=int)

            eye_bbox = [eye_center_x - ((eye_width + eye_width // 10) // 2),
                        eye_center_y - (eye_width // 3),
                        eye_width + eye_width // 10,
                        (eye_width // 3) * 2]

            roi = img[eye_bbox[0]:eye_bbox[0] + eye_bbox[2], eye_bbox[1]:eye_bbox[1] + eye_bbox[3]]

            # eyes = eye_cascade.detectMultiScale(roi_gray)
            # for (ex, ey, ew, eh) in eyes:
            #     cv.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)
            ex, ey, ew, eh = eye_bbox
            cv.rectangle(img, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)

        cv.imshow('img', img)

        if cv.waitKey(1) == ord('q'):
            break
    # When everything done, release the capture
    cap.release()
    cv.destroyAllWindows()
