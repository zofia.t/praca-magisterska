# Praca magisterska

Sterowanie kursorem myszy na podstawie analizy 'pola ruchu' oczu.<br /> 
*Controlling the mouse cursor based on the eyes 'optical flow' analysis.*

In *LK* and *HS* directories contain the final versions of the created algorithms. Folders contain requirements and user manual.