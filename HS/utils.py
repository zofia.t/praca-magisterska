import numpy as np
import cv2 as cv
import dlib


# For dlib’s 68-point facial landmark detector:
FACIAL_LANDMARKS_IDXS = {"mouth": (48, 68),
                         "inner_mouth": (60, 68),
                         "right_eyebrow": (17, 22),
                         "left_eyebrow": (22, 27),
                         "right_eye": (36, 42),
                         "left_eye": (42, 48),
                         "nose": (27, 36),
                         "jaw": (0, 17)}


def bbox_to_rect(bbox):
    # take bbox in format (x, y, w, h) - compatible with OpenCV
    # to dlib format
    return dlib.rectangle(bbox[0], bbox[1], bbox[2] + bbox[0], bbox[3] + bbox[1])


def shape_to_np(shape):
    # return the list of (x, y)-coordinates
    return np.array([(shape.part(i).x, shape.part(i).y) for i in range(shape.num_parts)], dtype=int)


def resize(image, scale_percent=500, inter=cv.INTER_AREA):
    # scale width and height of image
    width = int(image.shape[1] * scale_percent / 100)
    height = int(image.shape[0] * scale_percent / 100)

    # return the resized image
    return cv.resize(image, (width, height), interpolation=inter)


def resize_given_width(image, width=640, inter=cv.INTER_AREA):
    # scale height:
    scale = width / float(image.shape[0])
    height = int(image.shape[1] * scale)

    # return the resized image
    return cv.resize(image, (height, width), interpolation=inter), scale


def select_bbox_region_and_reshape(frame, bbox, return_scale=False):
    # select region of interest in order to bbox
    roi = frame[bbox[1]:bbox[1] + bbox[3],  # y:y+h
          bbox[0]:bbox[0] + bbox[2]]  # x:x+w

    # resize region
    resize_roi, scale = resize_given_width(roi)

    # return new region of interest
    if return_scale:  # and if needed also scale of resize
        return resize_roi, scale
    else:
        return resize_roi
