import pyautogui as cursor
import numpy as np

SCREEN_WIDTH, SCREEN_HEIGHT = cursor.size()
CENTER_X_POSITION = SCREEN_WIDTH // 2
CENTER_Y_POSITION = SCREEN_HEIGHT // 2
cursor.FAILSAFE = False


def init_cursor(x=CENTER_X_POSITION, y=CENTER_Y_POSITION):
    # set position to center or other given init position
    set_cursor_position(x, y)
    return cursor.position().x, cursor.position().y


def calculate_cursor_move(width_dist, height_dist, model):
    # Predict cursor move:
    # cursor_x_move = model[0].predict(np.array([[width_dist]]))[0]  # input must be 2d array, output is 1d array
    # cursor_y_move = model[1].predict(np.array([[height_dist]]))[0]

    # Or only multiply by coef of models
    cursor_x_move = model[0].coef_[0] * width_dist
    cursor_y_move = model[1].coef_[0] * height_dist

    # if calculate cursor move is too big and become a NAN number, then do not move at all:
    if np.isnan(cursor_x_move):
        cursor_x_move = 0
    if np.isnan(cursor_y_move):
        cursor_y_move = 0

    return cursor_x_move, cursor_y_move


def move_cursor(dx, dy):
    # if cursor move over the screen:
    if cursor.onScreen(cursor.position().x + dx, cursor.position().y + dy):
        cursor.move(dx, dy)

    # return new position of cursor
    return cursor.position().x, cursor.position().y


def set_cursor_position(x, y):
    # move cursor to selected position
    if cursor.onScreen(x, y):
        cursor.moveTo(x, y)


def perform_chosen_click_action(closed_eye_counter):
    if closed_eye_counter < 13:  # 3-13 frames (0.1-0.4 s)
        # Natural blinking
        return
    elif closed_eye_counter < 33:  # 16-33 frames (0.5-1 s)
        single_click()
        return

    elif closed_eye_counter < 100:  # 33-100 frames (1-3 s)
        double_click()
        return
    else:  # error
        return


def single_click():
    # do click on left button on mouse
    cursor.click()


def double_click():
    # do double click on left button on mouse
    cursor.doubleClick()




