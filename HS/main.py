from camera_eye_tracking import *
from controll_cursor import *
from calibration import calibration, get_dominant_eye, EAR_THRESHOLD, save_frame
import os
import pickle


if __name__ == '__main__':

    # Initialization
    # ----------------------------------------------------------------------------------------------------

    # init camera
    #  - real time movie:
    camera = cv.VideoCapture(1)  # 0 back, 1 front

    #  - from file
    # camera = cv.VideoCapture("./path_to_movie/movie_name.avi")

    # set image width and height
    camera.set(cv.CAP_PROP_FRAME_WIDTH, 1920)
    camera.set(cv.CAP_PROP_FRAME_HEIGHT, 1080)
    # ------------------------------------------------------------------------------------------------------

    # init cursor
    cursor_position = init_cursor()

    # get first two frames:
    frame = capture_frame(camera)  # capt
    # ------------------------------------------------------------------------------------------------------

    # initiate empty containers
    old_roi = np.empty((0, 0))  # for before roi region

    # bbox and rect of face:
    face_bbox = [0, 0, 0, 0]
    face_rect = utils.bbox_to_rect(face_bbox)

    # bbox of eyes and its width
    eye_bbox = [0, 0, 0, 0]
    eye_width = 0

    # points and number of them
    points = np.empty((0, 1, 2))
    num_pts = 0

    # counter of frames with closed eyes
    closed_eye_counter = 0

    # FIND FACE IN A FIRST FRAME
    # ------------------------------------------------------------------------------------------------------
    print("Localize face & face landmarks")
    err = 1
    while err:
        face_bbox, face_rect, err = detect_face(frame)

    # CALIBRATION
    # ------------------------------------------------------------------------------------------------------
    print('Calibration')
    calibration_dir = './calibration/'

    # if there is calibration file then load:
    if os.path.isdir(calibration_dir) and os.path.isfile(calibration_dir + 'all_models.pkl'):
        models = pickle.load(open(calibration_dir + 'all_models.pkl', 'rb'))
        dominant_eye, calibration_model = get_dominant_eye(models)
    else:  # create new calibration and save it
        dominant_eye, calibration_model = calibration(camera, frame, face_rect, out_dir=calibration_dir)

    # load EAR threshold from calibration file
    global EAR_THRESHOLD
    EAR_THRESHOLD = np.load(calibration_dir + 'ear_th.npy')[2]
    # ------------------------------------------------------------------------------------------------------

    # MAIN LOOP
    # ------------------------------------------------------------------------------------------------------
    try:
        while True:
            # get all landmarks on the face:
            all_landmarks, right_eye, left_eye = detects_landmarks(frame, face_rect)
            # get dominant eye landmarks
            eye_landmark = right_eye if 'right' in dominant_eye else left_eye
            # calculate current EAR
            ear_ratio = ear_ratio_calc(eye_landmark)

            if ear_ratio > EAR_THRESHOLD:
                # first check counter - if eyes was closed for enough time, maybe click should be performed
                perform_chosen_click_action(closed_eye_counter)
                # reset counter
                closed_eye_counter = 0

                if num_pts < 50:  # at first iteration
                    # get region of eye
                    eye_bbox, roi, eye_width = detects_roi(eye_landmark, frame, eye_width)

                    # initiate first move
                    _ = track_eye_movement(roi)

                    # set number of points to make this "if" statement false
                    num_pts = 100

                else:
                    # get region of interest on old and current frames:
                    roi = utils.select_bbox_region_and_reshape(frame, eye_bbox)

                    # detect movement in current frame
                    vx, vy = track_eye_movement(roi)

                    # calculate move of cursor in both x and y directions
                    cursor_dx, cursor_dy = calculate_cursor_move(vx, vy, calibration_model)
                    # move cursor
                    cursor_position_x, cursor_position_y = move_cursor(cursor_dx, cursor_dy)

            else:
                closed_eye_counter += 1  # increase the counter

            # get new frame
            old_frame, frame = capture_new_frame(frame, camera)

    except KeyboardInterrupt:
        pass

    # When everything done, release the capture
    camera.release()
    cv.destroyAllWindows()
