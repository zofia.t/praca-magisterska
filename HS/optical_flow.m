function [motion] = optical_flow(snap, w, h, opticFlow)
% reshape snap to correct format
snap_reshape = reshape(snap, [h, w]);
% rotate
snap_rotate = imrotate(snap_reshape, -90);
% estimate flow
flow = estimateFlow(opticFlow, snap_rotate);
% return sum of all moves in each axis
motion = struct('vx', sum(sum(flow.Vx)), 'vy',  sum(sum(flow.Vy)));
end

