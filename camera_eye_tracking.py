import numpy as np
import cv2 as cv
import dlib
import utils
from time import time

# init detectors
face_cascade = cv.CascadeClassifier('./haarcascades/haarcascade_frontalface_default.xml')
face_detector = dlib.get_frontal_face_detector()
landmark_predictor = dlib.shape_predictor("./shape_predictor_68_face_landmarks.dat")


def capture_frame(camera):
    # Check if camera is opened
    assert camera.isOpened(), "Camera is closed!"

    # Capture one frame
    ret, frame = camera.read()
    assert ret, "Can't receive frame"

    # flip in vertical directions
    frame = cv.flip(frame, 1)

    # return in gray scale
    return cv.cvtColor(frame, cv.COLOR_BGR2GRAY)


def capture_new_frame(frame, camera):
    old_frame = np.copy(frame)
    frame = capture_frame(camera)
    return old_frame, frame


def detect_face(frame):
    # detect face by Viola-Jones cascade
    face_bboxs = face_cascade.detectMultiScale(frame)
    # in any faces are detected
    if not (len(face_bboxs) == 0):
        # save region in two type
        # (one in dlib format and second for opencv/numpy)

        # save the biggest face that was detected
        face_bbox = face_bboxs[np.where(face_bboxs[:, 2] == np.max(face_bboxs[:, 2]))][0]
        face_rect = utils.bbox_to_rect(face_bbox)
        return face_bbox, face_rect, 0
    else:
        # detect face by HOG-based detector
        face_rects = face_detector(frame, 1)
        if not (len(face_rects) == 0):
            face_rect = face_rects[0]
            face_bbox = utils.rect_to_bbox(face_rect)
            return face_bbox, face_rect, 0
        else:
            return -1, -1, -1


def detects_landmarks(frame, face_rect):
    # detect face landmark at face region
    all_landmarks = landmark_predictor(frame, face_rect)
    # save them info numpy format
    all_landmarks = utils.shape_to_np(all_landmarks)

    # get right eye landmarks (left in image)
    (i, j) = utils.FACIAL_LANDMARKS_IDXS["left_eye"]
    right_eye_landmarks = all_landmarks[i:j]

    # get left eye landmarks (right in image)
    (i, j) = utils.FACIAL_LANDMARKS_IDXS["right_eye"]
    left_eye_landmarks = all_landmarks[i:j]

    # return all face landmark and right/left separately
    # all landmarks can be use to visualization
    return all_landmarks, right_eye_landmarks, left_eye_landmarks


def detects_roi(eye_landmarks, frame, eye_width=0):
    if eye_width == 0:
        # calculate the width of eye in order to max distance of points
        eye_width, eye_height = np.max(eye_landmarks, axis=0) - np.min(eye_landmarks, axis=0)

    # get the algebraic center of eye (position x and y)
    eye_center_x, eye_center_y = np.mean(eye_landmarks, axis=0, dtype=int)

    # save the bbox of eye: x, y, w, h
    # the box will be a little bigger then the eye, to make sure whole eye will be visible
    eye_bbox = [eye_center_x - ((eye_width + eye_width // 2) // 2),  # x position is more then 1/2 od width from center
                eye_center_y - ((eye_width + eye_width // 5) // 2),  # y position is calculated no matter of eye status
                eye_width + eye_width // 2,
                eye_width + eye_width // 5]

    # region of interest is the box with eye from the frame
    roi = utils.select_bbox_region_and_reshape(frame, eye_bbox)

    return eye_bbox, roi, eye_width


def ear_ratio_calc(eye_landmarks):
    p1_p4 = np.abs(eye_landmarks[0, 0] - eye_landmarks[3, 0])
    p2_p6 = np.abs(eye_landmarks[1, 1] - eye_landmarks[4, 1])
    p3_p5 = np.abs(eye_landmarks[2, 1] - eye_landmarks[5, 1])

    ear_ratio = (p2_p6 + p3_p5) / (2 * p1_p4)
    # print("ear_ratio: ", ear_ratio)
    return ear_ratio


def is_eye_open(eye_landmarks, threshold=0.1):
    if ear_ratio_calc(eye_landmarks) < threshold:
        return False
    else:
        return True


def detects_points_to_track(roi, mask=None):
    # set parameters of the function which determines corners
    feature_params = dict(maxCorners=1000,
                          qualityLevel=0.02,
                          minDistance=2,
                          blockSize=10,
                          useHarrisDetector=False,
                          k=0.04)

    # mask = create_iris_mask(roi, save=1,  out_dir=out_dir)
    # mask = create_iris_mask_rectangle(roi, scale, eye_landmark, save=1, out_dir=out_dir)
    # look for features around the iris
    points = cv.goodFeaturesToTrack(roi, mask=mask, **feature_params)

    return points


def create_iris_mask_rectangle(roi, scale, eye_landmark, eye_bbox, save=0, out_dir='./data/mov_1_out/'):
    eye_width, eye_height = np.max(eye_landmark, axis=0) - np.min(eye_landmark, axis=0)
    eye_center_frame_x, eye_center_frame_y = np.mean(eye_landmark, axis=0, dtype=int)

    eye_width_r = int(eye_width * scale)
    eye_height_r = int(eye_height * scale)
    eye_center_x = int((eye_center_frame_x - eye_bbox[0]) * scale)
    eye_center_y = int((eye_center_frame_y - eye_bbox[1]) * scale)

    mask = np.ones((roi.shape[0], roi.shape[1]), dtype=np.uint8) * 0

    start_x = eye_center_x - eye_width_r // 2
    start_y = eye_center_y - eye_height_r // 2
    end_x = eye_center_x + eye_width_r // 2
    end_y = eye_center_y + eye_height_r // 2

    mask = cv.rectangle(mask, (start_x, start_y), (end_x, end_y), (255, 255, 255), -1)
    if save:
        cv.imwrite(f'{out_dir}roi_mask.png', cv.add(roi, mask))

    return mask


def create_iris_mask(roi, save=0, out_dir='./data/mov_1_out/'):
    # threshold eye image
    roi_bw = np.copy(roi)
    roi_bw[roi_bw < 50] = 0  # get the darkest region and set to black
    roi_bw[roi_bw > 0] = 255  # make rest of it white

    # opening image to fill white points inside the iris (reflections of light)
    kernel = np.ones((40, 40), np.uint8)
    opening = cv.morphologyEx(roi_bw, cv.MORPH_OPEN, kernel)

    # dilatation to remove shadow of the upper eyelid
    kernel = np.ones((20, 20), np.uint8)
    dilation = cv.dilate(opening, kernel, iterations=4)

    # expand region, to get whole iris
    kernel = np.ones((20, 20), np.uint8)
    erode = cv.erode(dilation, kernel, iterations=4)

    # save the region of mask to check is whole iris is in it

    if save:
        cv.imwrite(f'{out_dir}roi.png', roi)
        cv.imwrite(f'{out_dir}roi_bw.png', roi_bw)
        cv.imwrite(f'{out_dir}opening.png', opening)
        cv.imwrite(f'{out_dir}dilation.png', dilation)
        cv.imwrite(f'{out_dir}erode.png', erode)
        cv.imwrite(f'{out_dir}roi_erode.png', cv.add(roi, erode))

    # mask is an opposite array to dilation - the iris is fill with "255" values, and the rest is "0"
    mask = np.ones_like(erode) * 255 - erode
    return mask


def track_eye_movement(frame, old_frame, old_points):
    # Parameters for Lucas Kanade optical flow
    lk_params = dict(winSize=(30, 30),  # window size
                     maxLevel=5,  # piramida level
                     criteria=(cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 10, 0.03))  # criteria of stop

    # calculate optical flow
    # start_1 = time()
    points, st, _ = cv.calcOpticalFlowPyrLK(old_frame, frame, old_points.astype(np.float32), None, **lk_params)
    # print(f'openCv flow: {round(time() - start_1, 4)} s')

    # Select good points
    good_new = points[st == 1].copy()
    # good_new = points.copy()
    good_new = np.reshape(good_new, (-1, 1, 2))

    good_old = old_points[st == 1].copy()
    # good_old = old_points.copy()
    good_old = np.reshape(good_old, (-1, 1, 2))
    num_pts = good_new.shape[0]

    return good_new, good_old, num_pts


