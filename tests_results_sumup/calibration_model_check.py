import numpy as np
import pickle
import matplotlib.pyplot as plt
from calibration import get_dominant_eye, prepare_models, CALIBRATION_POINTS
from controll_cursor import SCREEN_WIDTH, SCREEN_HEIGHT


print('Calibration')
# dominant_eye, calibration_model = calibration(camera, frame, face_rect, out_dir=out_dir)
calibration_dir = '../data/HS/calib/test_6/'

eye_movement_l = np.load(calibration_dir + 'eye_movement_l.npy')
eye_movement_r = np.load(calibration_dir + 'eye_movement_r.npy')
number_of_points = CALIBRATION_POINTS.shape[0]

reds = ['lightcoral', 'indianred', 'firebrick', 'darkred', 'red', 'tomato', 'darksalmon'] * 3
blues = ['paleturquoise', 'darkcyan', 'cyan', 'skyblue', 'deepskyblue', 'steelblue', 'cornflowerblue'] * 3
plt.figure()
for i in range(number_of_points):
    if np.isfinite(eye_movement_l[i, :]).all():
        plt.quiver(CALIBRATION_POINTS[i, 0], SCREEN_HEIGHT - CALIBRATION_POINTS[i, 1], eye_movement_l[i, 0], eye_movement_l[i, 1],
                   color=blues[i], label=f'l_{i}', units='xy', scale=1)
        plt.annotate(f'l_{i}', (CALIBRATION_POINTS[i, 0] + eye_movement_l[i, 0],
                                SCREEN_HEIGHT - CALIBRATION_POINTS[i, 1] + eye_movement_l[i, 1]))

    if np.isfinite(eye_movement_r[i, :]).all():
        plt.quiver(CALIBRATION_POINTS[i, 0], SCREEN_HEIGHT - CALIBRATION_POINTS[i, 1], eye_movement_r[i, 0], eye_movement_r[i, 1],
                   color=reds[i], label=f'r_{i}', units='xy', scale=1)
        plt.annotate(f'r_{i}', (CALIBRATION_POINTS[i, 0] + eye_movement_r[i, 0],
                                SCREEN_HEIGHT - CALIBRATION_POINTS[i, 1] + eye_movement_r[i, 1]))

plt.grid(True)
plt.show()

# models = prepare_models(eye_movement_l[1:], eye_movement_r[1:], number_of_points)

# with open(calibration_dir + 'all_models.pkl', 'wb') as f:
#     pickle.dump(models, f, pickle.HIGHEST_PROTOCOL)

models = pickle.load(open(calibration_dir + 'all_models.pkl', 'rb'))
models = models[2:]

plt.figure()
plt.subplot(211)
plt.title('width')
plt.plot(models[0]['x'][:, 0], models[0]['y'][:, 0], 'bo')
plt.plot(models[1]['x'][:, 0], models[1]['y'][:, 0], 'ro')
for i in range(models[0]['x'].shape[0]):
    plt.annotate(f'l_{i}', (models[0]['x'][i, 0], models[0]['y'][i, 0]))
    plt.annotate(f'r_{i}', (models[1]['x'][i, 0], models[1]['y'][i, 0]))
plt.xlabel('eye')
plt.ylabel('screen')
plt.grid(True)
plt.subplot(212)
plt.title('height')
plt.plot(models[0]['x'][:, 1], models[0]['y'][:, 1], 'bo')
plt.plot(models[1]['x'][:, 1], models[1]['y'][:, 1], 'ro')
for i in range(models[0]['x'].shape[0]):
    plt.annotate(f'l_{i}', (models[0]['x'][i, 1], models[0]['y'][i, 1]))
    plt.annotate(f'r_{i}', (models[1]['x'][i, 1], models[1]['y'][i, 1]))
plt.xlabel('eye')
plt.ylabel('screen')
plt.grid(True)
plt.show()

dominant_eye, calibration_model = get_dominant_eye(models)
print(calibration_model)

x = np.arange(-100, 100, 10).reshape(-1, 1)
y = np.zeros((len(models), x.shape[0], 2))
for m in range(len(models)):
    y[m, :, 0] = models[m]['model'][0].predict(x)
    y[m, :, 1] = models[m]['model'][1].predict(x)

colors = ['green', 'blue', 'red', 'yellow', 'orange', 'skyblue', 'black', 'magenta', 'lime']

fig, axs = plt.subplots(2, 2)

for m in range(len(models)):
    if models[m]['eye'] == 'right_eye':
        axs[0, 0].plot(x, y[m, :, 0], 'o-', color=colors[m], label=models[m]['regression_type'])
        axs[1, 0].plot(x, y[m, :, 1], 'o-', color=colors[m], label=models[m]['regression_type'])

    else:
        axs[0, 1].plot(x, y[m, :, 0], 'o-', color=colors[m], label=models[m]['regression_type'])
        axs[1, 1].plot(x, y[m, :, 1], 'o-', color=colors[m], label=models[m]['regression_type'])


axs[0, 0].set_title('width right eye')
axs[0, 0].set_xlabel('true')
axs[0, 0].set_ylabel('regression')
axs[0, 0].grid(True)
axs[0, 0].legend()

axs[0, 1].set_title('width left eye')
axs[0, 1].set_xlabel('true')
axs[0, 1].set_ylabel('regression')
axs[0, 1].grid(True)
axs[0, 1].legend()

axs[1, 0].set_title('height right eye')
axs[1, 0].set_xlabel('true')
axs[1, 0].set_ylabel('regression')
axs[1, 0].grid(True)
axs[1, 0].legend()

axs[1, 1].set_title('height left eye')
axs[1, 1].set_xlabel('true')
axs[1, 1].set_ylabel('regression')
axs[1, 1].grid(True)
axs[1, 1].legend()
fig.show()
