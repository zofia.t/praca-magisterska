import numpy as np
import matplotlib.pyplot as plt
import os
import pickle

# in_dir = '../data/HS/'
in_dir = '../data/LK/'

patterns = [p for p in os.listdir(in_dir) if os.path.isdir(in_dir + p) and p.startswith('wzor')]

times = {}
min_time = []
max_time = []
mean_time = []

plt.figure(figsize=(8, 4))
colors = ['r', 'g', 'b', 'm', 'c', 'y']
c = 0
for pattern in patterns:
    print(pattern)
    current_path = in_dir + pattern + '/'

    outs = [current_path + out + '/' for out in os.listdir(current_path)
            if os.path.isdir(current_path + out) and out.startswith('out')]

    times[pattern] = {}
    for i in range(len(outs)):
        print(f'movie {i}')
        times[pattern][i] = {}
        times_per_frame = np.load(outs[i] + "times_per_frame.npy")
        times[pattern][i] = {'times': times_per_frame,
                             'min': np.min(times_per_frame),
                             'max': np.max(times_per_frame),
                             'mean': np.mean(times_per_frame)}

        plt.plot(times_per_frame, color=colors[c], label=f'pomiar {c}')

        min_time.append(np.min(times_per_frame))
        max_time.append(np.max(times_per_frame))
        mean_time.append(np.mean(times_per_frame))

        print(f'min: {np.min(times_per_frame)} '
              f'\nmax: {np.max(times_per_frame)} '
              f'\nmean: {np.mean(times_per_frame)}')
        c += 1


plt.title('Czas przetwarzania zarejestrowany dla metody LK')
plt.grid()
plt.legend()
plt.xlabel('numer klatki nagrania')
plt.ylabel('czas [s]')
# plt.show()
plt.tight_layout()
plt.savefig(in_dir + 'times.png')
plt.close()
print('sum all:')
print(f'min: {np.round(np.min(min_time), 4)} '
      f'\nmax: {np.round(np.max(max_time), 4)} '
      f'\nmean: {np.round(np.mean(mean_time), 4)}')

with open(in_dir + 'times.pkl', 'wb') as f:
    pickle.dump(times, f, pickle.HIGHEST_PROTOCOL)
