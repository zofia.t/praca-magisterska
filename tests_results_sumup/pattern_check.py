import numpy as np
import matplotlib.pyplot as plt
# import cv2 as cv
import os
import pickle
from controll_cursor import *

in_dir = '../data/HS/'
# in_dir = '../data/LK/'
metoda = 'HS'
# metoda = 'LK'

patterns = [p for p in os.listdir(in_dir) if os.path.isdir(in_dir + p) and p.startswith('wzor')]

colors = ['g', 'b', 'm', 'c', 'y']

main_points = {'wzor_1':  [(1368, 912), 400],
               'wzor_2': [(968, 512), (968, 1312), (1768, 1312), (1768, 512)],
               'wzor_3': [(1368, 512), (968, 1312), (1768, 1312)]}


titles = {'wzor_1': f'Trajektoria kursora dla koła, metoda {metoda}',
          'wzor_2': f'Trajektoria kursora dla kwadratu, metoda {metoda}',
          'wzor_3': f'Trajektoria kursora dla trójkąta, metoda {metoda}'}

trajectories = {}
p = -1
for pattern in patterns:
    p += 1
    current_path = in_dir + pattern + '/'
    original = plt.imread(current_path + pattern + '.png')
    # original2 = cv.imread(current_path + pattern + '.png')

    # plt.figure(figsize=(original.shape[0]//200, original.shape[1]//200))
    plt.imshow(original)

    outs = [current_path + out + '/' for out in os.listdir(current_path)
            if os.path.isdir(current_path + out) and out.startswith('out')]
    trajectories[pattern] = {}
    for i in range(len(outs)):
        trajectory = np.load(outs[i] + 'cursor_trajectory.npy')
        # trajectory = trajectory[trajectory.nonzero()].reshape(-1, 2)
        trajectories[pattern][i] = {}
        trajectories[pattern][i]['trajectory'] = trajectory[1:]

        plt.plot(trajectory[:, 0], trajectory[:, 1], colors[i], label=f'nagranie {i + 1}')

    plt.title(titles[pattern])
    plt.xticks(np.arange(0, SCREEN_WIDTH + 70, step=400))
    plt.xlim([0, SCREEN_WIDTH])
    plt.yticks(np.arange(SCREEN_HEIGHT - 24, -1, step=-200))
    plt.ylim([SCREEN_HEIGHT, 0])
    plt.grid('both')
    plt.legend()
    # plt.show()
    plt.tight_layout()
    plt.savefig(current_path + 'trajectories_2.png')
    plt.close()

for p in trajectories.keys():
    for i in [0, 1]:
        movie_len = trajectories[p][i]['trajectory'].shape[0]

        if p == 'wzor_1':
            true_line = np.array([(main_points[p][0][0] + main_points[p][1] * np.cos(a),
                                   main_points[p][0][1] + main_points[p][1] * np.sin(a))
                                  for a in np.arange(- np.pi / 2, (2 * np.pi) - (np.pi / 2), (2 * np.pi) / movie_len)])
            end = np.repeat(main_points[p][0], movie_len - true_line.shape[0]).reshape(2, -1).T
            true_line = np.vstack([true_line, end])
            plt.plot(true_line[:, 0], true_line[:, 1], '.')

            trajectories[p][i]['true'] = true_line
        elif p == 'wzor_2':
            lines_len = movie_len // 4
            line_1 = np.array([np.ones(lines_len) * main_points[p][0][0],
                               np.arange(main_points[p][0][1], main_points[p][1][1],
                                         (main_points[p][1][1] - main_points[p][0][1]) / lines_len)])
            line_2 = np.array([np.arange(main_points[p][1][0], main_points[p][2][0],
                                         (main_points[p][2][0] - main_points[p][1][0]) / lines_len),
                               np.ones(lines_len) * main_points[p][1][1]])
            line_3 = np.array([np.ones(lines_len) * main_points[p][2][0],
                               np.arange(main_points[p][2][1], main_points[p][3][1],
                                         (main_points[p][3][1] - main_points[p][2][1]) / lines_len)])
            line_4 = np.array([np.arange(main_points[p][3][0], main_points[p][0][0],
                                         (main_points[p][0][0] - main_points[p][3][0]) / lines_len),
                               np.ones(lines_len) * main_points[p][3][1]])

            true_line = np.vstack([line_1.T, line_2.T, line_3.T, line_4.T])
            end = np.repeat(main_points[p][0], movie_len - true_line.shape[0]).reshape(2, -1).T
            true_line = np.vstack([true_line, end])
            plt.plot(true_line[:, 0], true_line[:, 1])

            trajectories[p][i]['true'] = true_line

        elif p == 'wzor_3':
            lines_len = movie_len // 3
            line_1 = np.array([np.arange(main_points[p][0][0], main_points[p][1][0],
                                         (main_points[p][1][0] - main_points[p][0][0]) / lines_len),
                               np.arange(main_points[p][0][1], main_points[p][1][1],
                                         (main_points[p][1][1] - main_points[p][0][1]) / lines_len)])
            line_2 = np.array([np.arange(main_points[p][1][0], main_points[p][2][0],
                                         (main_points[p][2][0] - main_points[p][1][0]) / lines_len),
                               np.ones(lines_len) * main_points[p][1][1]])
            line_3 = np.array([np.arange(main_points[p][2][0], main_points[p][0][0],
                                         (main_points[p][0][0] - main_points[p][2][0]) / lines_len),
                               np.arange(main_points[p][2][1], main_points[p][0][1],
                                         (main_points[p][0][1] - main_points[p][2][1]) / lines_len)])

            true_line = np.vstack([line_1.T, line_2.T, line_3.T])
            end = np.repeat(main_points[p][0], movie_len - true_line.shape[0]).reshape(2, -1).T
            true_line = np.vstack([true_line, end])
            plt.plot(true_line[:, 0], true_line[:, 1])

            trajectories[p][i]['true'] = true_line

with open(in_dir + 'trajectories.pkl', 'wb') as f:
    pickle.dump(trajectories, f, pickle.HIGHEST_PROTOCOL)
