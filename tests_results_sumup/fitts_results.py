import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

in_dir = '../data/LK/fitts/'
tests = sorted([d for d in os.listdir(in_dir) if os.path.isdir(in_dir + d)])

all_results = pd.DataFrame()
for test in tests:
    result = pd.read_csv(in_dir + test + '/' + 'results.csv')
    all_results = pd.concat([all_results, result])

all_results.reset_index(drop=True, inplace=True)
all_results['test_nr'] = all_results.index

columns_with_name_error = [k for k in all_results.keys() if k.endswith(' ')]
for col in columns_with_name_error:
    all_results[col[:-1]] = all_results[col]
    all_results.drop(columns=[col], inplace=True)

columns = all_results.keys().to_list()
test_descriptions = [c for c in columns if c[0].isdecimal() and c.endswith('(ms)')]
wides = np.array([float(des.split('_')[0][:-2]) for des in test_descriptions])
distances = np.array([float(des.split('_')[2][:-2]) for des in test_descriptions])
tests_id = np.log2(distances/wides + 1)

tests_id_map = {f'{int(wides[i])}cm_X_{int(distances[i])}cm' if wides[i] > 0.5
                else f'{wides[i]}cm_X_{int(distances[i])}cm': round(tests_id[i], 2)
                for i in range(len(tests_id))}

_ = [print(f'{k}: {v}') for k, v in tests_id_map.items()]

tests_d_w_map = {f'{int(wides[i])}cm_X_{int(distances[i])}cm' if wides[i] > 0.5
                 else f'{wides[i]}cm_X_{int(distances[i])}cm': (wides[i], distances[i])
                 for i in range(len(wides))}

all_results.to_csv(in_dir + 'all_results.csv')

# times plots:
times_df = pd.melt(all_results, id_vars=['Name', 'Total_Time(ms)', 'test_nr'],
                   value_vars=[col for col in columns if col.endswith('Avg_Time(ms)')],
                   var_name='test_name', value_name='time')
times_df['test_name'] = [name[:-13] for name in times_df['test_name'].values]
times_df['ID'] = [tests_id_map[name] for name in times_df['test_name'].values]
times_df['Użytkownik'] = ['1' if name == 'Zofia' else '2' for name in times_df['Name'].values]

print(f"mean times: \n{times_df.groupby(['test_name', 'ID', 'Name'])['time'].mean()}")

times_df['wides'] = [tests_d_w_map[name][0] for name in times_df['test_name'].values]
times_df['distances'] = [tests_d_w_map[name][1] for name in times_df['test_name'].values]
times_df['time_mean'] = times_df.groupby(['wides', 'distances', 'Name'])['time'].transform('mean')
mean_times_df = times_df.loc[:, ['wides', 'distances', 'time_mean', 'Name']].drop_duplicates()
times_pivot_1 = mean_times_df.loc[mean_times_df['Name'] == 'Zofia', :].pivot("wides", "distances", "time_mean")

fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, figsize=(10, 5))
sns.heatmap(times_pivot_1, annot=True, fmt="0.2f", linewidths=.5, cmap='coolwarm',
            vmin=400, vmax=900, ax=ax1, cbar=False)
ax1.set_title('Średni czas [ms] - użytkownik 1.')
ax1.set_xlabel('Odległość [cm]')
ax1.set_ylabel('Szerokość [cm]')
# plt.savefig(in_dir + 'mean_times_heatmap_1.png')
# plt.close()

times_pivot_2 = mean_times_df.loc[mean_times_df['Name'] == 'Stefan', :].pivot("wides", "distances", "time_mean")

# plt.figure()
sns.heatmap(times_pivot_2, annot=True, fmt="0.2f", linewidths=.5, cmap='coolwarm',
            vmin=400, vmax=900, ax=ax2, yticklabels=False)
ax2.set_title('Średni czas [ms] - użytkownik 2.')
ax2.set_xlabel('Odległość [cm]')
ax2.set_ylabel('')
plt.tight_layout()
plt.savefig(in_dir + 'mean_times_heatmaps.png')
plt.close()

plt.figure()
sns.boxplot(x="ID", y="time",
            # hue="Użytkownik", palette=["b", "g"],
            data=times_df.loc[times_df.loc[:, 'Użytkownik'] == '1'])
sns.stripplot(x="ID", y="time", data=times_df.loc[times_df.loc[:, 'Użytkownik'] == '1'],
              size=4, color=".3", linewidth=0)
plt.grid(True)

plt.xlabel('współczynnik trudności (ID)')
plt.ylabel('średni czas [ms]')
plt.savefig(in_dir + 'times.png')
plt.close()


# error plots:
error_df = pd.melt(all_results, id_vars=['Name', 'Total_Error(%)', 'test_nr'],
                   value_vars=[col for col in columns if col.endswith('Errors(%)')],
                   var_name='test_name', value_name='error')
error_df['test_name'] = [name[:-10] for name in error_df['test_name'].values]
error_df['ID'] = [tests_id_map[name] for name in error_df['test_name'].values]
error_df['Użytkownik'] = ['1' if name == 'Zofia' else '2' for name in error_df['Name'].values]

print(f"mean error: \n{error_df.groupby(['test_name', 'ID', 'Name'])['error'].mean()}")

error_df['wides'] = [tests_d_w_map[name][0] for name in error_df['test_name'].values]
error_df['distances'] = [tests_d_w_map[name][1] for name in error_df['test_name'].values]
error_df['mean_error'] = error_df.groupby(['wides', 'distances', 'Name'])['error'].transform('mean')
mean_error_df = error_df.loc[:, ['wides', 'distances', 'mean_error', 'Name']].drop_duplicates()
error_pivot_1 = mean_error_df.loc[mean_error_df['Name'] == 'Zofia', :].pivot("wides", "distances", "mean_error")

fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, figsize=(10, 5))
sns.heatmap(error_pivot_1, annot=True, fmt="0.0f", linewidths=.5,
            vmin=50, vmax=100, cmap='coolwarm', ax=ax1, cbar=False)
ax1.set_title('Średni błąd [%] - użytkownik 1.')
ax1.set_xlabel('Odległość [cm]')
ax1.set_ylabel('Szerokość [cm]')
# plt.savefig(in_dir + 'mean_error_heatmap_1.png')
# plt.close()

error_pivot_2 = mean_error_df.loc[mean_error_df['Name'] == 'Stefan', :].pivot("wides", "distances", "mean_error")

# plt.figure()
sns.heatmap(error_pivot_2, annot=True, fmt="0.0f", linewidths=.5,
            vmin=50, vmax=100, cmap='coolwarm', ax=ax2, yticklabels=False)
ax2.set_title('Średni błąd [%] - użytkownik 2.')
ax2.set_xlabel('Odległość [cm]')
ax2.set_ylabel('')
plt.tight_layout()
plt.savefig(in_dir + 'mean_error_heatmaps.png')
plt.close()

plt.figure()
sns.boxplot(x="ID", y="error",
            # hue="Użytkownik", palette=["b", "g"],
            data=error_df.loc[error_df.loc[:, 'Użytkownik'] == '1'])
sns.stripplot(x="ID", y="error", data=error_df.loc[error_df.loc[:, 'Użytkownik'] == '1'],
              size=4, color=".3", linewidth=0)
plt.grid(True)

plt.xlabel('współczynnik trudności (ID)')
plt.ylabel('błąd [%]')
plt.savefig(in_dir + 'errors.png')
plt.close()
