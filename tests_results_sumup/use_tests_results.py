import os
import numpy as np
import matplotlib.pyplot as plt
from controll_cursor import SCREEN_WIDTH, SCREEN_HEIGHT


in_dir = '../data/LK/use_test/'
tests = sorted([d for d in os.listdir(in_dir) if os.path.isdir(in_dir + d) if not d.endswith('1')])

chrome_xy = {'test_2': (165, 730),
             'test_3': (123, 1469),
             'test_4': (230, 700),
             'test_5': (675, 1777),
             'test_6': (34, 544),
             'test_7': (537, 1780)}

for test in tests:
    current_path = in_dir + test + '/'
    original = plt.imread(current_path + 'pulpit.png')
    plt.imshow(original)

    trajectory = np.load(current_path + 'cursor_trajectory.npy')
    time_per_frame = np.load(current_path + 'times_per_frame.npy')

    x, y = chrome_xy[test]
    end_idx = np.argmin(np.power(trajectory[:, 0] - x, 2) + np.power(trajectory[:, 1] - y, 2)) + 1

    # plt.plot(trajectory[:, 0], trajectory[:, 1], label='all')
    # plt.plot(trajectory[:120, 0], trajectory[:120, 1], label='120')
    # plt.plot(trajectory[:90, 0], trajectory[:90, 1], label='90')
    # plt.plot(trajectory[:60, 0], trajectory[:60, 1], label='60')
    # plt.plot(trajectory[:30, 0], trajectory[:30, 1], label='30')
    plt.plot(trajectory[:end_idx, 0], trajectory[:end_idx, 1], label='end', color='b')

    end_time = np.sum(time_per_frame[:end_idx])
    plt.text(2200, 100, f'czas: {round(end_time, 3)} [s]', size='medium',
             **{'horizontalalignment': 'center', 'verticalalignment': 'baseline'})

    print(f'{test}: {round(end_time, 4)} [s]')

    # plt.legend()
    plt.xlim([0, SCREEN_WIDTH])
    plt.ylim([SCREEN_HEIGHT, 0])
    plt.xticks([], [])
    plt.yticks([], [])
    plt.savefig(in_dir + test + '_trajectories_best_time.png')
    plt.close()
    # plt.show()

