import numpy as np
import matplotlib.pyplot as plt
import os
import pickle

in_dirs = ['./data/LK/', './data/HS/']

for in_dir in in_dirs:
    print(in_dir)
    trajectories = pickle.load(open(in_dir + 'trajectories.pkl', 'rb'))
    precision = {}

    for p in trajectories.keys():
        precision[p] = {}
        print(p)

        plt.figure()
        for i in [0, 1]:
            print(f'movie {i}')
            diff = np.abs(trajectories[p][i]['true'] - trajectories[p][i]['trajectory'])
            plt.plot(diff[:, 0], label=f'film {i} - x')
            plt.plot(diff[:, 1], label=f'film {i} - y')
            precision[p][i] = {'difference': diff,
                               'max': np.max(diff),
                               'mean': np.mean(diff)}

            print(f'max: {np.max(diff)} \nmean: {np.mean(diff)}')

        plt.title(p)
        plt.legend()
        plt.grid()
        plt.show()

    with open(in_dir + 'precision.pkl', 'wb') as f:
        pickle.dump(precision, f, pickle.HIGHEST_PROTOCOL)


