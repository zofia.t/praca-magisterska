function [motion] = optical_flow(snap, w, h, opticFlow)

snap_reshape = reshape(snap, [h, w]);
snap_rotate = imrotate(snap_reshape, -90);
flow = estimateFlow(opticFlow, snap_rotate);
% vx =  median(flow.Vx, 'all');
% vy = median(flow.Vy, 'all');
% vx = flow.Vx((flow.Vx > 1e-02) | (flow.Vx < -1e-02));
% vy = flow.Vy((flow.Vy > 1e-02) | (flow.Vy < -1e-02));
% if isempty(vx)
%     vx = 0;
% end
% if isempty(vy)
%     vy = 0;
% end
% motion = struct('vx', sum(vx), 'vy',  sum(vy));
motion = struct('vx', sum(sum(flow.Vx)), 'vy',  sum(sum(flow.Vy)));
end

