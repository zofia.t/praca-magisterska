from camera_eye_tracking import *
from controll_cursor import *
from calibration import calibration, get_dominant_eye, EAR_THRESHOLD, save_frame
import visualization_functions as vis
from time import sleep, time
import os
import pickle


if __name__ == '__main__':

    # Initialization
    # ----------------------------------------------------------------------------------------------------

    # init camera
    camera = cv.VideoCapture(1)  # 0 back, 1 front

    # camera = cv.VideoCapture("./data/LK/fitts/test_1/blinking_only.mp4")
    camera.set(cv.CAP_PROP_FRAME_WIDTH, 1920)
    camera.set(cv.CAP_PROP_FRAME_HEIGHT, 1080)

    out_dir = './data/LK/test_abc/'

    os.makedirs(out_dir, exist_ok=True)
    # ------------------------------------------------------------------------------------------------------
    # Save out video
    fourcc = cv.VideoWriter_fourcc(*'DIVX')
    w = int(camera.get(cv.CAP_PROP_FRAME_WIDTH))
    h = int(camera.get(cv.CAP_PROP_FRAME_HEIGHT))
    fps = camera.get(cv.CAP_PROP_FPS)
    out = cv.VideoWriter(out_dir + 'output.avi', fourcc, 30.0, (w, h))
    # ------------------------------------------------------------------------------------------------------

    # init cursor
    cursor_position = init_cursor()  # wzor 1 i 3: x=1368, y=512, wzor 2: x=968, y=512
    # cursor_trajectory = np.zeros((int(camera.get(cv.CAP_PROP_FRAME_COUNT)) - 1, 2))
    cursor_trajectory = np.zeros((18000, 2))  # 10 minutes
    cursor_trajectory[0] = cursor_position

    # get first frame:
    frame = capture_frame(camera)  # capt
    save_frame(frame, out)
    # ------------------------------------------------------------------------------------------------------

    # initiate empty containers
    old_roi = np.empty((0, 0))  # for before roi region

    # bbox and rect of face:
    face_bbox = [0, 0, 0, 0]
    face_rect = utils.bbox_to_rect(face_bbox)

    # bbox of eyes and its width
    eye_bbox = [0, 0, 0, 0]
    eye_width = 0

    # points and number of them
    points = np.empty((0, 1, 2))
    num_pts = 0

    # counter of points detections:
    counter_points = 0

    # counter of frames with closed eyes
    closed_eye_counter = 0

    # time checking
    # time_per_frame = np.zeros(int(camera.get(cv.CAP_PROP_FRAME_COUNT)) - 1)
    time_per_frame = np.zeros(18000)  # 10 minutes
    # ------------------------------------------------------------------------------------------------------

    print("Localize face & face landmarks")
    err = 1
    while err:
        face_bbox, face_rect, err = detect_face(frame)

    # ------------------------------------------------------------------------------------------------------
    # Calibration
    print('Calibration')
    # dominant_eye, calibration_model = calibration(camera, frame, face_rect, out_dir=out_dir)
    calibration_dir = './data/LK/calib/test_3/'
    models = pickle.load(open(calibration_dir + 'all_models.pkl', 'rb'))
    models = models[2:]
    dominant_eye, calibration_model = get_dominant_eye(models)

    print(f'models gains: \n  x: {round(calibration_model[0].coef_[0], 4)} '
          f'\n  y: {round(calibration_model[1].coef_[0], 4)}')
    global EAR_THRESHOLD
    EAR_THRESHOLD = np.load(calibration_dir + 'ear_th.npy')[2]

    # dominant_eye = 'right_eye'
    # calibration_model = None
    # ------------------------------------------------------------------------------------------------------

    # MAIN LOOP
    # ------------------------------------------------------------------------------------------------------
    # for it in range(0, int(camera.get(cv.CAP_PROP_FRAME_COUNT)) - 2):
    for it in range(0, 300):

    # it = 0
    # while True:
    #     it += 1
        start_time = time()
        # print(f"i={it}")
        all_landmarks, right_eye, left_eye = detects_landmarks(frame, face_rect)
        eye_landmark = right_eye if 'right' in dominant_eye else left_eye
        ear_ratio = ear_ratio_calc(eye_landmark)

        if ear_ratio > EAR_THRESHOLD:
            # first check counter - if eyes was closed for enough time, maybe click should be performed
            perform_chosen_click_action(closed_eye_counter)
            # reset counter
            closed_eye_counter = 0
            # print(f"Number of points: {num_pts}")
            eye_bbox, roi, eye_width = detects_roi(eye_landmark, frame, eye_width)

            # If there is no point to track, then detect
            if num_pts < 50:
                print(f"Localize points of iris to track, iteration: {it}, num_pts: {num_pts}")
                counter_points += 1
                eye_bbox, roi, eye_width = detects_roi(eye_landmark, frame, eye_width)

                if roi.shape[0] > 0:
                    # get points to track
                    # print("Detection")
                    # points = detects_points_to_track(roi, out_dir)
                    roi_scale = utils.select_bbox_region_and_reshape(frame, eye_bbox, return_scale=True)

                    points = detects_points_to_track(roi=roi,
                                                     mask=create_iris_mask_rectangle(roi=roi,
                                                                                     scale=roi_scale[1],
                                                                                     eye_landmark=eye_landmark,
                                                                                     eye_bbox=eye_bbox,
                                                                                     save=0,
                                                                                     out_dir=out_dir))

                    if points is None:
                        points = np.empty((0, 1, 2))
                    num_pts = points.shape[0]

                    # visualization:
                    # vis.show_points_on_frame(roi, points, ear_ratio, it, out_dir)
            else:
                # print("Cursor moving")
                old_roi = utils.select_bbox_region_and_reshape(old_frame, eye_bbox)

                roi = utils.select_bbox_region_and_reshape(frame, eye_bbox)
                # get the new and old roi and points to detect movement
                points, old_points, num_pts = track_eye_movement(roi, old_roi, points)

                cursor_dx, cursor_dy = calculate_cursor_move(points, old_points, calibration_model)
                cursor_position_x, cursor_position_y = move_cursor(cursor_dx, cursor_dy)
                cursor_trajectory[it] = (cursor_position_x, cursor_position_y)
                # visualization:
                # vis.show_new_ang_old_points_on_frame(roi, points, old_points, ear_ratio, it, out_dir)
        else:
            closed_eye_counter += 1  # counting only
            roi = utils.select_bbox_region_and_reshape(frame, eye_bbox)

            # visualization:
            # vis.show_frame(roi, ear_ratio, it, closed=1, out_dir=out_dir)

        time_per_frame[it] = time() - start_time

        # get new frame
        old_frame, frame = capture_new_frame(frame, camera)
        save_frame(frame, out)
        if cv.waitKey(1) == ord('q'):
            break

    # print(f"points detections: {counter_points}")

    # print(f"max time: {np.max(time_per_frame)}")
    # print(f"min time: {np.min(time_per_frame)}")
    # print(f"mean time: {np.mean(time_per_frame)}")
    time_per_frame = time_per_frame[time_per_frame.nonzero()]
    np.save(out_dir + "times_per_frame.npy", time_per_frame)

    # print(f"cursor trajectory: {cursor_trajectory}")
    cursor_trajectory = cursor_trajectory[cursor_trajectory.nonzero()].reshape(-1, 2)
    np.save(out_dir + "cursor_trajectory.npy", cursor_trajectory)

    # When everything done, release the capture
    camera.release()
    out.release()

    cv.destroyAllWindows()
