import cv2 as cv
import numpy as np


def visualize(frame, face_bbox, all_landmarks, eye_bbox, points):
    img = np.copy(frame)

    name = "frame"
    cv.namedWindow(name)  # Create a named window
    cv.moveWindow(name, 0, 0)  # Move it to upper left corner

    # face rectangle:
    x, y, w, h = face_bbox
    cv.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)

    # face landmark
    for (x, y) in all_landmarks:
        cv.circle(img, (x, y), 1, (0, 0, 255), -1)

    # eye rectangle:
    ex, ey, ew, eh = eye_bbox
    cv.rectangle(img, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)

    if points is not None:
        # points to track:
        for n in range(points.shape[0]):
            px, py = points[n][0]
            cv.circle(img, (int(px + x), int(py + y)), 2, (100, 25, 200), 1)

    # Create a mask image for drawing purposes
    # mask = np.zeros_like(old_frame)
    #
    # # draw the tracks
    # for i, (new, old) in enumerate(zip(good_new, good_old)):
    #     a, b = new.ravel()
    #     c, d = old.ravel()
    #     mask = cv.line(mask, (a, b), (c, d), color[i].tolist(), 2)
    #     frame = cv.circle(frame, (a, b), 5, color[i].tolist(), -1)
    # img = cv.add(frame, mask)

    cv.imshow(name, img)
    # cv.imwrite(f'{out_dir}image.png', img)


def show_frame(roi, ear_ratio, i, closed=1, out_dir='./data/mov_1_out/'):
    name = "eye"
    cv.namedWindow(name)  # Create a named window
    cv.moveWindow(name, 0, 0)  # Move it to upper left corner

    cv.putText(roi, f"{i}; EAR ratio: {ear_ratio}", (10, 20), cv.FONT_HERSHEY_SIMPLEX, 0.5, (209, 80, 0, 255), 1)
    if closed:
        cv.putText(roi, "EYE CLOSED!!!!!!!!", (20, 80), cv.FONT_HERSHEY_SIMPLEX, 2, (209, 80, 0, 255), 2)
    cv.imshow(name, roi)

    # cv.imwrite(f'{out_dir}{i}.png', roi)


def show_points_on_frame(roi, points, ear_ratio, i, out_dir='./data/mov_1_out/'):
    name = "eye"
    cv.namedWindow(name)  # Create a named window
    cv.moveWindow(name, 0, 0)  # Move it to upper left corner

    for x, y in points[:, 0, :]:
        roi = cv.circle(roi, (x, y), 5, (100, 25, 200), -1)
    cv.putText(roi, f"{i}; EAR ratio: {ear_ratio}", (10, 20), cv.FONT_HERSHEY_SIMPLEX, 0.5,
               (209, 80, 0, 255), 1)
    cv.imshow(name, roi)
    # cv.imwrite(f'{out_dir}{i}.png', roi)


def show_new_ang_old_points_on_frame(roi, points, old_points, ear_ratio, i, out_dir='./data/mov_1_out/'):
    name = "eye"
    cv.namedWindow(name)  # Create a named window
    cv.moveWindow(name, 0, 0)  # Move it to upper left corner

    # Create a mask image for drawing purposes
    mask = np.zeros_like(roi)

    # draw the tracks
    for _, (new, old) in enumerate(zip(points[:, 0, :], old_points[:, 0, :])):
        mask = cv.line(mask, (new[0], new[1]), (old[0], old[1]), (255, 255, 0), 2)
        roi = cv.circle(roi, (new[0], new[1]), 5, (255, 0, 0), -1)
    roi = cv.add(roi, mask)

    cv.putText(roi, f"{i}; EAR ratio: {ear_ratio}", (10, 20), cv.FONT_HERSHEY_SIMPLEX, 0.5, (209, 80, 0, 255), 1)
    cv.imshow(name, roi)
    # cv.imwrite(f'{out_dir}{i}.png', roi)
