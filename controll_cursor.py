import pyautogui as cursor
import numpy as np

SCREEN_WIDTH, SCREEN_HEIGHT = cursor.size()
CENTER_X_POSITION = SCREEN_WIDTH // 2
CENTER_Y_POSITION = SCREEN_HEIGHT // 2
cursor.FAILSAFE = False


def init_cursor(x=CENTER_X_POSITION, y=CENTER_Y_POSITION):
    # set position to center or other given init position
    cursor.moveTo(x, y)
    return cursor.position().x, cursor.position().y


def calculate_cursor_move(new_points, old_points, model):
    dists = (new_points - old_points).reshape(-1, 2)
    width_dist, height_dist = np.median(dists, 0)
    # print(f'eye move x: {round(width_dist, 4)}')
    # print(f'eye move y: {round(height_dist, 4)}')
    # cursor_x_move = model[0].predict(np.array([[width_dist]]))[0]  # input must be 2d array, output is 1d array
    # cursor_y_move = model[1].predict(np.array([[height_dist]]))[0]
    cursor_x_move = model[0].coef_[0] * width_dist
    cursor_y_move = model[1].coef_[0] * height_dist
    # print(f'cursor move x: {round(cursor_x_move, 2)}')
    # print(f'cursor move y: {round(cursor_y_move, 2)}')
    if np.isnan(cursor_x_move):
        cursor_x_move = 0
    if np.isnan(cursor_y_move):
        cursor_y_move = 0
    return cursor_x_move, cursor_y_move


def calculate_cursor_move_v2(width_dist, height_dist, model):
    # print(f'eye move x: {round(width_dist, 4)}')
    # print(f'eye move y: {round(height_dist, 4)}')
    # cursor_x_move = model[0].predict(np.array([[width_dist]]))[0]  # input must be 2d array, output is 1d array
    # cursor_y_move = model[1].predict(np.array([[height_dist]]))[0]
    cursor_x_move = model[0].coef_[0] * width_dist
    cursor_y_move = model[1].coef_[0] * height_dist
    # print(f'cursor move x: {round(cursor_x_move, 4)}')
    # print(f'cursor move y: {round(cursor_y_move, 4)}')
    return cursor_x_move, cursor_y_move


def move_cursor(dx, dy):
    if cursor.onScreen(cursor.position().x + dx, cursor.position().y + dy):
        # print('Move cursor')
        cursor.move(dx, dy)
    else:
        pass
        # print(f'point ({round(cursor.position().x + dx, 0)}, '
        #       f'{round(cursor.position().y + dy, 0)})'
        #       f' not on screen {SCREEN_WIDTH, SCREEN_HEIGHT}')
    return cursor.position().x, cursor.position().y


def set_cursor_position(x, y):
    # move cursor to selected position
    if cursor.onScreen(x, y):
        cursor.moveTo(x, y)


def perform_chosen_click_action(closed_eye_counter):
    if closed_eye_counter < 13:  # 3-13 frames (0.1-0.4 s)
        # print(f"Eye is closed - natural blinking, iteration: {it}")
        print('Natural blinking')
        return
    elif closed_eye_counter < 33:  # 16-33 frames (0.5-1 s)
        # print(f"Eye is closed - unnatural blinking, iteration: {it}")
        print('Click!')
        single_click()
        return

    elif closed_eye_counter < 100:  # 33-100 frames (1-3 s)
        # print(f"Eye is closed - long blinking, iteration: {it}")
        print('Double click!')
        double_click()
        return
    else:
        # print(f"Eye is closed ?????, iteration: {it}")
        return


def single_click():
    # do click on left button on mouse
    cursor.click()


def double_click():
    # do double click on left button on mouse
    cursor.doubleClick()




