import numpy as np
import cv2 as cv
import dlib
import utils

# init detectors
face_cascade = cv.CascadeClassifier('./haarcascade_frontalface_default.xml')
landmark_predictor = dlib.shape_predictor("./shape_predictor_68_face_landmarks.dat")


def capture_frame(camera):
    # Check if camera is opened
    assert camera.isOpened(), "Camera is closed!"

    # Capture one frame
    ret, frame = camera.read()
    assert ret, "Can't receive frame"

    # flip in vertical directions
    frame = cv.flip(frame, 1)

    # return in gray scale
    return cv.cvtColor(frame, cv.COLOR_BGR2GRAY)


def save_frame(frame, out_movie):
    # Undo colors translations and direction
    frame = cv.cvtColor(frame, cv.COLOR_GRAY2BGR)
    frame = cv.flip(frame, 1)

    # add frame to output movie
    out_movie.write(frame)
    return


def capture_new_frame(frame, camera):
    old_frame = np.copy(frame)  # copy current frame
    frame = capture_frame(camera)  # capture new
    return old_frame, frame


def detect_face(frame):
    # detect face by Viola-Jones cascade
    face_bboxs = face_cascade.detectMultiScale(frame)
    # in any faces are detected
    if not (len(face_bboxs) == 0):
        # save region in two type
        # (one in dlib format and second for opencv/numpy)

        # save the biggest face that was detected
        face_bbox = face_bboxs[np.where(face_bboxs[:, 2] == np.max(face_bboxs[:, 2]))][0]
        face_rect = utils.bbox_to_rect(face_bbox)
        return face_bbox, face_rect, 0

    else:  # if none face return "error code"
        return -1, -1, -1


def detects_landmarks(frame, face_rect):
    # detect face landmark at face region
    all_landmarks = landmark_predictor(frame, face_rect)
    # save them info numpy format
    all_landmarks = utils.shape_to_np(all_landmarks)

    # get right eye landmarks (left in image)
    (i, j) = utils.FACIAL_LANDMARKS_IDXS["left_eye"]
    right_eye_landmarks = all_landmarks[i:j]

    # get left eye landmarks (right in image)
    (i, j) = utils.FACIAL_LANDMARKS_IDXS["right_eye"]
    left_eye_landmarks = all_landmarks[i:j]

    # return all face landmark and right/left separately
    # all landmarks can be use to visualization
    return all_landmarks, right_eye_landmarks, left_eye_landmarks


def detects_roi(eye_landmarks, frame, eye_width=0):
    if eye_width == 0:  # in a first iteration there is no eye_width
        # calculate the width of eye in order to max distance of points
        eye_width, eye_height = np.max(eye_landmarks, axis=0) - np.min(eye_landmarks, axis=0)

    # get the algebraic center of eye (position x and y)
    eye_center_x, eye_center_y = np.mean(eye_landmarks, axis=0, dtype=int)

    # save the bbox of eye: x, y, w, h
    # the box will be a little bigger then the eye, to make sure whole eye will be visible
    eye_bbox = [eye_center_x - ((eye_width + eye_width // 2) // 2),  # x position is more then 1/2 od width from center
                eye_center_y - ((eye_width + eye_width // 5) // 2),  # y position is calculated no matter of eye status
                eye_width + eye_width // 2,
                eye_width + eye_width // 5]

    # region of interest is the box with eye from the frame
    roi = utils.select_bbox_region_and_reshape(frame, eye_bbox)

    return eye_bbox, roi, eye_width


def ear_ratio_calc(eye_landmarks):
    # Calculate absolute difference (only in one axis)
    p1_p4 = np.abs(eye_landmarks[0, 0] - eye_landmarks[3, 0])  # width of eye (only in x)
    p2_p6 = np.abs(eye_landmarks[1, 1] - eye_landmarks[4, 1])  # high of eye (in y)
    p3_p5 = np.abs(eye_landmarks[2, 1] - eye_landmarks[5, 1])  # high of eye (in y)

    # Calculate ratio from formula
    ear_ratio = (p2_p6 + p3_p5) / (2 * p1_p4)
    return ear_ratio


def detects_points_to_track(roi, mask=None):
    # set parameters of the function which determines corners
    feature_params = dict(maxCorners=1000,
                          qualityLevel=0.02,
                          minDistance=2,
                          blockSize=10,
                          useHarrisDetector=False,
                          k=0.04)

    # look for features around the iris
    points = cv.goodFeaturesToTrack(roi, mask=mask, **feature_params)

    return points


def create_iris_mask(roi, scale, eye_landmark, eye_bbox):
    # get current width and height of eye from eye landmarks
    eye_width, eye_height = np.max(eye_landmark, axis=0) - np.min(eye_landmark, axis=0)
    # calculate co-ordinates center of eye
    eye_center_frame_x, eye_center_frame_y = np.mean(eye_landmark, axis=0, dtype=int)

    # multiply by scale to find points in eye region (not whole image)
    eye_width_r = int(eye_width * scale)
    eye_height_r = int(eye_height * scale)
    eye_center_x = int((eye_center_frame_x - eye_bbox[0]) * scale)
    eye_center_y = int((eye_center_frame_y - eye_bbox[1]) * scale)

    # prepare array of mask will with zeros
    mask = np.zeros((roi.shape[0], roi.shape[1]), dtype=np.uint8)

    # calculate start and ends points of rectangle of eye
    start_x = eye_center_x - eye_width_r // 2
    start_y = eye_center_y - eye_height_r // 2
    end_x = eye_center_x + eye_width_r // 2
    end_y = eye_center_y + eye_height_r // 2

    # create rectangle
    mask = cv.rectangle(mask, (start_x, start_y), (end_x, end_y), (255, 255, 255), -1)

    return mask


def track_eye_movement(frame, old_frame, old_points):
    # Parameters for Lucas Kanade optical flow
    lk_params = dict(winSize=(30, 30),  # window size
                     maxLevel=5,  # piramida level
                     criteria=(cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 10, 0.03))  # criteria of stop

    # calculate optical flow
    points, st, _ = cv.calcOpticalFlowPyrLK(old_frame, frame, old_points.astype(np.float32), None, **lk_params)

    # Select points that was found on new frame
    # for new points
    good_new = points[st == 1].copy()
    good_new = np.reshape(good_new, (-1, 1, 2))
    # and old ones
    good_old = old_points[st == 1].copy()
    good_old = np.reshape(good_old, (-1, 1, 2))

    # calculate current number of points
    num_pts = good_new.shape[0]

    return good_new, good_old, num_pts


