BEFORE RUN:

1. Install Python 3.6
2. Install packages from requirements.txt by running command in system prompt:
	pip install -r requirements.txt

3. Download Haar Cascade for frontal face from OpenCV repo 
   and save in the same directory as all the program files:
	
	https://github.com/opencv/opencv/blob/master/data/haarcascades/haarcascade_frontalface_default.xml

4. Download Landmarks Predictor from dlib site,
   save in the same directory as all the program files and unpack:

	http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2

PROGRAM STRUCTURE:

1. main.py 
	Main program. Inside You can select camera (real time of from file). 
	If there are no calibration files then the program will start calibration.
	After one use, the calibration will be saved and read from files.
	Program controls the cursor on the screen by analyzing the eye movement.
	Click ones by closing eyes for 0.5-1s. Double-click by closing eyes for 1-3s.

2. calibration.py
	File contains all functions used during calibration. You can run this file seperatly.
	This file contain "main" module. By running it You will start calibration. 
	The results will be saved and read by main.py. 

3. controll_cursor.py
	Contains all the functions for controlling the cursor. 
	The calculation of movement, moves, single and double clicks.

4. camera_eye_tracking.py
	Contains all the functions for running the camera (getting frame), finding face on image,
	finding landmarks and read eye movement by detecting points and tracking it by Optical Flow.

5. utils.py
	Few extra functions to translate formats from dlib to OpenCV and reshaping and rezising images.

ADDITIONAL INFORMATION FOR USER:
1. The camera should be installed above the monitor.
2. The camera should be able to read images in 1920x1080 format and sample with a frequency of 30 fps.
3. The user's face should be frontal facing the camera.
4. The user's eyes should be at the height of the camera and 40 cm away from it.
5. The user's face should be evenly illuminated.
6. The user should rest their head on the chin rest.