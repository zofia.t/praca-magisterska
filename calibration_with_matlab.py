from camera_eye_tracking_with_matlab import matlab, capture_frame, capture_new_frame, detect_face, detects_landmarks
from camera_eye_tracking_with_matlab import detects_roi, ear_ratio_calc
from controll_cursor import CENTER_X_POSITION, CENTER_Y_POSITION, SCREEN_WIDTH, SCREEN_HEIGHT
import utils
import cv2 as cv
import numpy as np
import pickle
import os
from copy import deepcopy
from sklearn.linear_model import LinearRegression

EAR_THRESHOLD = 0.25
RADIUS = 20
CALIBRATION_POINTS = np.array([[CENTER_X_POSITION, CENTER_Y_POSITION],
                               [0 + (2 * RADIUS), 0 + (2 * RADIUS)],
                               [SCREEN_WIDTH - (2 * RADIUS), 0 + (2 * RADIUS)],
                               [SCREEN_WIDTH - (2 * RADIUS), SCREEN_HEIGHT - (2 * RADIUS)],
                               [CENTER_X_POSITION, CENTER_Y_POSITION],
                               [0 + (2 * RADIUS), SCREEN_HEIGHT - (2 * RADIUS)],
                               [CENTER_X_POSITION, CENTER_Y_POSITION],
                               [CENTER_X_POSITION // 2, CENTER_Y_POSITION // 2],
                               [CENTER_X_POSITION, CENTER_Y_POSITION // 2],
                               [CENTER_X_POSITION + (CENTER_X_POSITION // 2), CENTER_Y_POSITION // 2],
                               [CENTER_X_POSITION + (CENTER_X_POSITION // 2), CENTER_Y_POSITION],
                               [CENTER_X_POSITION + (CENTER_X_POSITION // 2),
                                CENTER_Y_POSITION + (CENTER_Y_POSITION // 2)],
                               [CENTER_X_POSITION, CENTER_Y_POSITION + (CENTER_Y_POSITION // 2)],
                               [CENTER_X_POSITION // 2, CENTER_Y_POSITION + (CENTER_Y_POSITION // 2)],
                               [CENTER_X_POSITION // 2, CENTER_Y_POSITION]])

# init matlab and optical flows:
eng = matlab.engine.start_matlab()
# HS in matlab
opticFlow_l = eng.opticalFlowHS('Smoothness', 1.8, 'MaxIteration', 5)
opticFlow_r = eng.opticalFlowHS('Smoothness', 1.8, 'MaxIteration', 5)

# show desktop
# eng.desktop(nargout=0)


def calibration(camera, frame, face_rect, out_movie,
                number_of_points=CALIBRATION_POINTS.shape[0], out_dir='./data/HS/calib/'):
    global EAR_THRESHOLD
    # INIT
    # ---------------------------------------------------------------------
    _, right_eye, left_eye = detects_landmarks(frame, face_rect)

    # Left eye init:
    eye_bbox_l, _, _ = detects_roi(left_eye, frame, 0)
    eye_movement_l = np.zeros((number_of_points, 2))

    # Right eye init:
    eye_bbox_r, _, _ = detects_roi(right_eye, frame, 0)
    eye_movement_r = np.zeros((number_of_points, 2))

    closed = True
    closed_eye_counter = 0

    # Create a named window
    calib_win_name = "calibration"
    cv.namedWindow(calib_win_name, cv.WINDOW_NORMAL)
    # Set full screen
    cv.setWindowProperty(calib_win_name, cv.WND_PROP_FULLSCREEN, cv.WINDOW_FULLSCREEN)

    # blank white image in size of screen
    show_point = np.ones((SCREEN_HEIGHT, SCREEN_WIDTH, 3), np.uint8) * 255
    cv.imshow(calib_win_name, show_point)
    for i in range(3):  # for 0.1s white screen
        cv.imshow(calib_win_name, show_point)
        frame = capture_frame(camera)
        save_frame(frame, out_movie)
        if cv.waitKey(1) == ord('q'):
            break

    # ---------------------------------------------------------------------
    # Calculate EAR threshold
    EAR_THRESHOLD = calculate_ear_threshold(camera, face_rect, frame, show_point, out_movie, out_dir)
    old_frame, frame = capture_new_frame(frame, camera)
    save_frame(frame, out_movie)
    # ---------------------------------------------------------------------

    # LOOP over calibration points
    for n in range(number_of_points):
        # select point on screen to be reach be eye
        point_to_reach = CALIBRATION_POINTS[n]
        # visualize point
        visualize_calibration_point(point_to_reach, np.copy(show_point), calib_win_name)
        if cv.waitKey(1) == ord('q'):
            break
        print(f'point to reach: {point_to_reach}')
        reach_point = 0

        # Wait until open eyes
        while closed:
            # print('waiting to open eyes')
            # get the eyes landmarks
            _, right_eye, left_eye = detects_landmarks(frame, face_rect)
            closed = check_if_eye_closed(left_eye, right_eye)

            # get new frame
            old_frame, frame = capture_new_frame(frame, camera)
            save_frame(frame, out_movie)

        # initiate first move
        _ = get_move(frame, eye_bbox_l, opticFlow_l)
        _ = get_move(frame, eye_bbox_r, opticFlow_r)

        # Loop to reach point - eyes closed with enough time
        while (not reach_point) or (not closed_eye_counter):
            closed = check_if_eye_closed(left_eye, right_eye)

            if not closed:
                # If eyes are open:
                closed_eye_counter = 0

                # track points
                distance_l = get_move(frame, eye_bbox_l, opticFlow_l)
                distance_r = get_move(frame, eye_bbox_r, opticFlow_r)

                # add movement
                eye_movement_l[n] = np.sum([eye_movement_l[n], distance_l], axis=0)
                eye_movement_r[n] = np.sum([eye_movement_r[n], distance_r], axis=0)

            else:
                # If eye are closed
                closed_eye_counter += 1

                if (closed_eye_counter > 3) and (closed_eye_counter < 13):  # 3-13 frames (0.1-0.4 s) - natural blinking
                    pass  # nothing happening
                elif (closed_eye_counter > 16) and (closed_eye_counter < 33):  # 16-33 frames (0.5-1 s) - unnatural blinking
                    reach_point = 1
                    print('point reached')
                elif (closed_eye_counter > 33) and (closed_eye_counter < 100):  # 33-100 frames (1-3 s) long blinking
                    pass  # nothing happening
                else:   # really long blinking - something is wrong
                    pass

            # get the eyes landmarks
            _, right_eye, left_eye = detects_landmarks(frame, face_rect)

            # get new frame
            if camera.get(cv.CAP_PROP_POS_FRAMES) != camera.get(cv.CAP_PROP_FRAME_COUNT):
                old_frame, frame = capture_new_frame(frame, camera)
                save_frame(frame, out_movie)

        # show move of both eyes
        print(f'movement left: {eye_movement_l[n]}\nmovement right: {eye_movement_r[n]}')

    # close calibration points
    cv.destroyWindow(calib_win_name)

    np.save(f'{out_dir}eye_movement_l.npy', eye_movement_l)
    np.save(f'{out_dir}eye_movement_r.npy', eye_movement_r)
    np.save(f'{out_dir}calibration_points.npy', CALIBRATION_POINTS)

    # prepare models in order to distances between points
    models = prepare_models(eye_movement_l[1:], eye_movement_r[1:], number_of_points)
    # save_models
    with open(out_dir + 'all_models.pkl', 'wb') as f:
        pickle.dump(models, f, pickle.HIGHEST_PROTOCOL)

    # in order to score of the models return the best one and the dominant eye
    # dominant_eye, calibration_model = get_dominant_eye(models)
    return get_dominant_eye(models)  # dominant_eye, calibration_model


def get_move(frame, eye_bbox, opticFlow):
    roi = utils.select_bbox_region_and_reshape(frame, eye_bbox)

    flow = eng.optical_flow(roi.tobytes(), int(roi.shape[0]), int(roi.shape[1]), opticFlow)
    vx, vy = flow.values()
    return np.array([vx, vy])


def save_frame(frame, out_movie, save=True):
    if save:
        frame = cv.cvtColor(frame, cv.COLOR_GRAY2BGR)
        frame = cv.flip(frame, 1)
        out_movie.write(frame)
    return


def calculate_ear_threshold(camera, face_rect, frame, show_point, out_movie, out_dir='./data/HS/calib/test/',
                            ear_calibration_time=60):  # 60 frames = 2 s
    # First get the ear for open eyes
    ear_open, frame = get_ear_th_for_open_or_closed_eyes(camera, ear_calibration_time, face_rect, frame,
                                                         out_movie, show_point, gesture='open')

    ear_closed, frame = get_ear_th_for_open_or_closed_eyes(camera, ear_calibration_time, face_rect, frame,
                                                           out_movie, show_point, gesture='closed')

    ear_th = ear_closed + (ear_open - ear_closed) / 6
    print(f'EAR threshold {ear_th}')
    np.save(f'{out_dir}ear_th.npy', np.array([ear_open, ear_closed, ear_th]))
    return ear_th


def get_ear_th_for_open_or_closed_eyes(camera, ear_calibration_time, face_rect, frame, out_movie, show_point, gesture):
    if gesture == 'open':
        command = "Keep yours eyes wide open for 2s"
        out_ear_function = np.max
    elif gesture == 'closed':
        command = "Keep yours eyes closed for 2s"
        out_ear_function = np.min
    else:  # error
        return -1, frame

    ear_buffer = np.zeros(ear_calibration_time)
    for i in range(ear_calibration_time):  # for 2s period
        visualize_calibration_command(np.copy(show_point), command)
        # cv.imwrite(f'{out_dir}{gesture}.png', frame)

        # get the eyes landmarks
        _, right_eye, left_eye = detects_landmarks(frame, face_rect)
        ear_buffer[i] = get_mean_ear_ratio_for_both_eyes(left_eye, right_eye)
        # get new frame
        frame = capture_frame(camera)
        save_frame(frame, out_movie)

        if cv.waitKey(1) == ord('q'):
            break

    # cv.imwrite(f'{out_dir}last_{gesture}.png', frame)

    ear_th = out_ear_function(ear_buffer)  # get EAR
    print(f'EAR {gesture} eyes: {ear_th}')
    for i in range(3):  # for 0.1s black screen
        cv.imshow("calibration", np.copy(show_point) * 0)
        frame = capture_frame(camera)
        save_frame(frame, out_movie)

        if cv.waitKey(1) == ord('q'):
            break
    return ear_th, frame


def check_if_eye_closed(left_eye, right_eye):
    # calculate EAR ratio
    ear_ratio = get_mean_ear_ratio_for_both_eyes(left_eye, right_eye)

    return ear_ratio < EAR_THRESHOLD


def visualize_calibration_point(point_to_reach, show_point, name="calibration"):
    # Add red point to screen
    show_point = cv.circle(show_point, (point_to_reach[0], point_to_reach[1]), RADIUS, (0, 0, 255), -1)
    cv.imshow(name, show_point)


def visualize_calibration_command(show_point, command, name="calibration", font=cv.FONT_HERSHEY_SIMPLEX):
    # get boundary of this text
    command_size = cv.getTextSize(command, font, 2, 2)[0]

    # get coords based on boundary
    command_x = (show_point.shape[1] - command_size[0]) // 2
    command_y = (show_point.shape[0] + command_size[1]) // 2

    # Show command on center of screen
    cv.putText(show_point, f"{command}", (command_x, command_y), font, 2, (0, 0, 0), 2)
    cv.imshow(name, show_point)


def get_mean_ear_ratio_for_both_eyes(left_eye, right_eye):
    # get EAR ratio for each eye
    ear_ratio_l = ear_ratio_calc(left_eye)
    ear_ratio_r = ear_ratio_calc(right_eye)
    # calculated mean EAR
    # ear_ratio = np.mean([ear_ratio_l, ear_ratio_r])
    # return ear_ratio
    return np.max([ear_ratio_l, ear_ratio_r])


def prepare_models(eye_movement_l, eye_movement_r, number_of_points):
    # get the distances between points on screen and reach by each eye
    dist_on_screen = get_distances_of_points(CALIBRATION_POINTS[:number_of_points])

    # exclude not reach movements:
    dist_on_screen_l, eye_movement_l = exclude_not_reach_points(dist_on_screen, eye_movement_l)
    dist_on_screen_r, eye_movement_r = exclude_not_reach_points(dist_on_screen, eye_movement_r)

    # generate linear regression model for each eye
    regressions = {'no_calib': LinearRegression(),
                   'linear': LinearRegression(fit_intercept=False),
                   # 'Bayesian': BayesianRidge(fit_intercept=False),
                   # 'GLM_normal': TweedieRegressor(power=0),
                   # 'GLM_poisson': TweedieRegressor(power=1),
                   # 'GLM_gamma': TweedieRegressor(power=2),
                   # 'gradient': GradientBoostingRegressor(random_state=1),
                   # 'random_forest': RandomForestRegressor(random_state=1)
                   }
    models = []
    for reg_name, reg in regressions.items():
        if reg_name.startswith('no'):

            # pack them into list
            models.append([{'model': get_calibration_model(dist_on_screen_l, dist_on_screen_l, reg),
                            'x': dist_on_screen_l,
                            'y': dist_on_screen_l,
                            'eye': 'left_eye',
                            'regression_type': reg_name},
                           {'model': get_calibration_model(dist_on_screen_r, dist_on_screen_r, reg),
                            'x': dist_on_screen_r,
                            'y': dist_on_screen_r,
                            'eye': 'right_eye',
                            'regression_type': reg_name}])
        else:
            # pack them into list
            models.append([{'model': get_calibration_model(dist_on_screen_l, eye_movement_l, reg),
                            'x': eye_movement_l,
                            'y': dist_on_screen_l,
                            'eye': 'left_eye',
                            'regression_type': reg_name},
                           {'model': get_calibration_model(dist_on_screen_r, eye_movement_r, reg),
                            'x': eye_movement_r,
                            'y': dist_on_screen_r,
                            'eye': 'right_eye',
                            'regression_type': reg_name}])

    return sum(models, [])


def exclude_not_reach_points(dist_on_screen, eye_movement, nan_idx=np.empty(0), zeros_idx=np.empty(0)):
    # get idx where are 'nan'
    if not np.isfinite(eye_movement).all():
        nan_idx = np.where(np.isfinite(eye_movement) == False)[0]
    # get idx where are '0'
    if np.isclose(eye_movement, 0).any():
        zeros_idx = np.where(np.isclose(eye_movement, 0) == True)[0]

    # sum idx to exclude
    to_exclude = np.unique(np.hstack([nan_idx, zeros_idx]))

    # get only that points/ vector that exist and are correct
    eye_movement_out = np.array([eye_movement[idx, :] for idx in range(eye_movement.shape[0])
                                 if idx not in to_exclude])
    dist_on_screen_out = np.array([dist_on_screen[idx, :] for idx in range(dist_on_screen.shape[0])
                                   if idx not in to_exclude])

    return dist_on_screen_out, eye_movement_out


def get_distances_of_points(points):
    # separate x and y dimension
    width = points[:, 0]
    height = points[:, 1]

    # get distances between the points (width and height separately)
    width_dist = np.diff(width)
    height_dist = np.diff(height)

    return np.array([width_dist, height_dist]).T


def get_calibration_model(distances_on_screen, distances, regression=LinearRegression()):
    # get x, y points for width
    width_x = distances[:, 0].reshape(-1, 1)
    width_y = distances_on_screen[:, 0]

    # get x, y points for height
    height_x = distances[:, 1].reshape(-1, 1)
    height_y = distances_on_screen[:, 1]

    # add (0.0, 0.0) point to both width and height:
    width_x = np.vstack([width_x, 0.0])
    width_y = np.hstack([width_y, 0.0])
    height_x = np.vstack([height_x, 0.0])
    height_y = np.hstack([height_y, 0.0])

    # made copy of regression instance
    model_width = deepcopy(regression)
    model_height = deepcopy(regression)

    # calculate regression for each (width and height) dimension
    model_width = model_width.fit(width_x, width_y)
    model_height = model_height.fit(height_x, height_y)
    return model_width, model_height


def get_dominant_eye(models):
    # prepare array to contain the models scores
    scores = np.zeros(len(models))

    # get score for each model:
    for i in range(len(models)):
        scores[i] = get_score_of_model(models[i]['x'], models[i]['y'], models[i]['model'])

    print('scores:')
    _ = [print(models[i]["regression_type"] + ' ' + models[i]["eye"] + ": " + str(scores[i]))
         for i in range(len(models))]
    # the biggest score is the best model and the dominant eye is the corresponding one
    return models[np.argmax(scores)]['eye'], models[np.argmax(scores)]['model']


def get_score_of_model(x, y, model, width=0, height=1):
    score_width = model[width].score(x[:, width].reshape(-1, 1), y[:, width])
    score_height = model[height].score(x[:, height].reshape(-1, 1), y[:, height])
    # return the mean score for each direction
    return np.mean([score_width, score_height])


if __name__ == '__main__':
    # INITIALIZATION
    # ----------------------------------------------------------------------------------------------------
    # init camera
    # camera = cv.VideoCapture(1)
    camera = cv.VideoCapture("./data/LK/calib/test_3/movie.avi")
    camera.set(cv.CAP_PROP_FRAME_WIDTH, 1920)
    camera.set(cv.CAP_PROP_FRAME_HEIGHT, 1080)

    output_dir = './data/HS/calib/test_6/'
    os.makedirs(output_dir, exist_ok=True)

    # Save out video
    fourcc = cv.VideoWriter_fourcc(*'DIVX')
    w = int(camera.get(cv.CAP_PROP_FRAME_WIDTH))
    h = int(camera.get(cv.CAP_PROP_FRAME_HEIGHT))
    fps = camera.get(cv.CAP_PROP_FPS)
    out = cv.VideoWriter(output_dir + 'movie.avi', fourcc, 30.0, (w, h))

    # get first frame:
    frame = capture_frame(camera)  # capt
    save_frame(frame, out)

    # bbox and rect of face:
    face_bbox = [0, 0, 0, 0]
    face_rect = utils.bbox_to_rect(face_bbox)

    # FIND FACE IN A FIRST FRAME
    # ----------------------------------------------------------------------------------------------------
    err = 1
    while err:
        face_bbox, face_rect, err = detect_face(frame)

    # CALIBRATION
    # ----------------------------------------------------------------------------------------------------
    dominant_eye, calibration_model = calibration(camera, frame, face_rect, out, out_dir=output_dir)
    # ----------------------------------------------------------------------------------------------------

    # while camera.isOpened():
    #     frame = capture_frame(camera)  # capt
    #     # save_frame(frame, out)
    #     _, right_eye, left_eye = detects_landmarks(frame, face_rect)
    #     # Left eye init:
    #     eye_bbox_l, _, _ = detects_roi(left_eye, frame, 0)
    #     # Right eye init:
    #     eye_bbox_r, _, _ = detects_roi(right_eye, frame, 0)
    #
    #     roi_l = utils.select_bbox_region_and_reshape(frame, eye_bbox_l)
    #     roi_r = utils.select_bbox_region_and_reshape(frame, eye_bbox_r)
    #
    #     cv.imwrite(f'{output_dir}frame.png', frame)
    #
    #     cv.imwrite(f'{output_dir}left_eye.png', roi_l)
    #     cv.imwrite(f'{output_dir}right_eye.png', roi_r)
    #
    #     # cv.imshow('frame', frame)
    #     if cv.waitKey(1) == ord('q'):
    #         break

    camera.release()
    cv.destroyAllWindows()
